use std::{fs, env};
use chrono::{Datelike, Timelike, Local};
use rust_utils::utils::run_command;

const MONTHS: [&str; 12] = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
];

const VERSION: &str = env!("CARGO_PKG_VERSION");

fn main() {
    println!("cargo:rerun-if-changed=build.rs");

    // get the current date and time (formatted)
    let date = Local::now().date_naive();
    let month = MONTHS[date.month() as usize - 1];
    let day = date.day();
    let year = date.year();
    let date_str = format!("{day} {month} {year}");
    let time = Local::now().time();
    let hour = time.hour();
    let minute = time.minute();
    let second = time.second();
    let time_str = format!("{hour:02}:{minute:02}:{second:02}");

    // manpage templates
    let gatewaymon_manpage = format!(
        ".\\\" Manpage for gatewaymon (part of T-Mobile Home Internet Tools).\n\
        .\\\" Created on {month} {day}, {year} at {time_str}\n\
        .TH gatewaymon 1 \"{date_str}\" \"{VERSION}\" \"gatewaymon man page\"\n\
        .SH NAME\n\
        gatewaymon \\- T-Mobile gateway monitor.\n\
        .SH DESCRIPTION\n\
        This process monitors T-Mobile Home Internet gateways, keeps a history of the signal strength and reboots the gateway if it causes any connection trouble.\n\n\
        .SH EXAMPLES\n\
        start from systemd: systemctl start --user gatewaymon\n\
        .br\n\
        start upon first login using systemd: systemctl enable --user gatewaymon\n\
        .br\n\
        start manually (as background process): gatewaymon &\n\
        .br\n\
        Show debug log info: gatewaymon -d &\n\n\
        .SH SEE ALSO\n\
        tmotop(1)\n\
        .br\n\
        tmocli(1)\n\n\
        .SH AUTHOR\n\
        Noah Jelen (noahtjelen@gmail.com)"
    );
    
    let tmocli_manpage = format!(
        ".\\\" Manpage for tmocli (part of T-Mobile Home Internet Tools).\n\
        .\\\" Created on {month} {day}, {year} at {time_str}\n\
        .TH tmocli 1 \"{date_str}\" \"{VERSION}\" \"tmocli man page\"\n\
        .SH NAME\n\
        tmocli \\- Command line interface for controlling T-Mobile Home Internet gateways\n\n\
        .SH SYNOPSIS\n\
        tmocli -h\n\
        .br\n\
        tmocli -v\n\
        .br\n\
        tmocli info\n\
        .br\n\
        tmocli reboot\n\
        .br\n\
        tmocli stats\n\
        .br\n\
        tmocli connection\n\
        .br\n\
        tmocli data (nokia gateway only)\n\
        .br\n\
        tmocli networks\n\
        .br\n\
        tmocli login\n\
        .br\n\
        tmocli devices\n\
        .br\n\
        tmocli device\n\
        .br\n\n\
        .SH DESCRIPTION\n\
        Allows controlling of T-Mobile gateways from the command line. Please make sure that gatewaymon is not running when using this to reboot the gateway!\n\n\
        .SH OPTIONS\n\
        -h : Show help\n\
        .br\n\
        -v : Show current version\n\
        .br\n\
        info: Show info about the gateway\n\
        .br\n\
        reboot: Reboot the gateway. Pass -y to skip the confirmation prompt.   \n\
        .br\n\
        stats: Show all the stats of the gateway\n\
        .br\n\
        connection: Show connection stats\n\
        .br\n\
        networks: Show network stats\n\
        .br\n\
        pass -q to show wifi QR codes (sagemcom gateway only)\n\
        .br\n\
        data: Show data usage stats (nokia gateway only)\n\
        .br\n\
        login: Log into gateway\n\
        .br\n\
        devices: List devices connected to gateway\n\
        .br\n\
        device: Get info about a connected device by its name IP address, or MAC address\n\n\
        .SH EXAMPLES\n\
        .br\n\
        Reboot gateway: tmocli reboot\n\
        .br\n\
        Show all stats: tmocli stats\n\n\
        .SH SEE ALSO\n\
        gatewaymon(1)\n\
        .br\n\
        tmotop(1)\n\n\
        .SH AUTHOR\n\
        Noah Jelen (noahtjelen@gmail.com)"
    );

    let tmotop_manpage = format!(
        ".\\\" Manpage for tmotop (part of T-Mobile Home Internet Tools).\n\
        .\\\" Created on {month} {day}, {year} at {time_str}\n\
        .TH tmotop 1 \"{date_str}\" \"{VERSION}\" \"tmotop man page\"\n\
        .SH NAME\n\
        tmotop \\- Text user interface that shows information about a connected T-Mobile gateway.\n\
        .SH DESCRIPTION\n\
        The interface shows the signal strength, active networks, data usage (nokia only), and connected devices.\n\n\
        .SH SEE ALSO\n\
        tmotop(1)\n\
        .br\n\
        gatewaymon(1)\n\n\
        .SH AUTHOR\n\
        Noah Jelen (noahtjelen@gmail.com)"
    );

    // save the manpages
    fs::remove_dir_all("man/").unwrap_or(());
    fs::create_dir_all("man/").unwrap_or(());
    fs::write("man/gatewaymon.1", gatewaymon_manpage).expect("Unable to save manpages!");
    fs::write("man/tmocli.1", tmocli_manpage).expect("Unable to save manpages!");
    fs::write("man/tmotop.1", tmotop_manpage).expect("Unable to save manpages!");

    if env::set_current_dir("aur/").is_ok() {
        // Arch PKGBUILD template
        let aur_pkgbuild = format!(
            "# Maintainer: Noah Jelen <noahtjelen@gmail.com>\n\
            # Generated by cargo on {month} {day}, {year} at {time_str}\n\
            pkgname=tmobile-internet-tools\n\
            pkgver={VERSION}\n\
            pkgrel=1\n\
            pkgdesc=\"Set of tools for working with T-Mobile Home Internet gateways\"\n\
            arch=('i686' 'x86_64')\n\
            url=\"https://gitlab.com/NoahJelen/tmobile-internet-tools\"\n\
            license=('GPL3')\n\
            depends=('ncurses' 'gcc-libs' 'glibc' 'networkmanager' 'openssl')\n\
            optdepends=('libnotify: Desktop notifications about gateway issues')\n\
            makedepends=('cargo')\n\
            source=(\"https://gitlab.com/NoahJelen/tmobile-internet-tools/-/archive/$pkgver/tmobile-internet-tools-$pkgver.zip\")\n\
            md5sums=('SKIP')\n\n\
            build() {{\n    \
                cd \"tmobile-internet-tools-$pkgver\"\n    \
                # Feature configuration\n    \
                # add --features nokia for nokia gateway support\n    \
                # add --no-default-features to disable sagemcom support\n    \
                cargo build --release\n    \
                cd target/release\n    \
                ln -sf gatewaymon tmotop\n    \
                ln -sf gatewaymon tmocli\n\
            }}\n\
            \npackage() {{\n    \
                cd \"tmobile-internet-tools-$pkgver\"\n    \
                mkdir -p \"$pkgdir/usr/share/tmobile-internet-tools/\"\n    \
                cp -r webui/ \"$pkgdir/usr/share/tmobile-internet-tools/\"\n    \
                chmod -R 755 \"$pkgdir/usr/share/tmobile-internet-tools/webui/\"\n    \
                mkdir -p \"$pkgdir/usr/share/man/man1/\"\n    \
                mkdir -p \"$pkgdir/usr/lib/systemd/system\"\n    \
                install -Dt \"$pkgdir/usr/bin\" -m755 target/release/gatewaymon\n    \
                install -Dt \"$pkgdir/usr/bin\" -m755 target/release/tmocli\n    \
                install -Dt \"$pkgdir/usr/bin\" -m755 target/release/tmotop\n    \
                install -Dt \"$pkgdir/usr/bin\" -m755 target/release/tmo-webui\n    \
                install -Dt \"$pkgdir/usr/share/man/man1\" man/gatewaymon.1\n    \
                install -Dt \"$pkgdir/usr/share/man/man1\" man/tmocli.1\n    \
                install -Dt \"$pkgdir/usr/share/man/man1\" man/tmotop.1\n    \
                install -Dt \"$pkgdir/usr/lib/systemd/user\" gatewaymon.service\n    \
                install -Dt \"$pkgdir/usr/lib/systemd/system\" gatewaymon.service\n    \
                install -Dt \"$pkgdir/usr/lib/systemd/user\" gatewaymon-debug.service\n    \
                install -Dt \"$pkgdir/usr/lib/systemd/system\" gatewaymon-debug.service\n    \
                install -Dt \"$pkgdir/usr/lib/systemd/user\" tmo-webui.service\n    \
                install -Dt \"$pkgdir/usr/lib/systemd/system\" tmo-webui.service\n\
            }}"
        );

        // save the PKGBUILD
        fs::write("PKGBUILD", aur_pkgbuild).unwrap_or(());

        // use makepkg to generate a .SRCINFO file from the aur pkgbuild
        let srcinfo = run_command("makepkg", false, ["--printsrcinfo"]).output;
        fs::write(".SRCINFO", srcinfo).unwrap();
    }
}