use super::*;
use std::{
    cmp::Ordering,
    collections::HashSet
};
use serde_derive::{Serialize, Deserialize};
use crate::LOG_DIR;

const HEADER: &str =
    "│Time │Connected?│LTE Signal                        │5G Signal                         │Rebooted?│\n\
     ├─────┼──────────┼──────────────────────────────────┼──────────────────────────────────┼─────────┤\n";
const DIV: &str = "├─────┼──────────┼──────────────────────────────────┼──────────────────────────────────┼─────────┤\n";
const TAIL: &str = "└─────┴──────────┴──────────────────────────────────┴──────────────────────────────────┴─────────┘";

// gateway signal tracker
// hopefully this will be able to help
// me find out what the hell is going
// on with this thing's connectivity!
#[derive(Serialize, Deserialize, Clone, Eq, PartialEq)]
pub struct SignalTracker(Vec<SignalTrackerEntry>);

impl SignalTracker {
    pub fn get(archive: bool) -> SignalTracker {
        let encoded = match fs::read(format!("{}/signal_tracker", *LOG_DIR)) {
            Ok(f) => f,
            Err(_) => return SignalTracker(vec![])
        };

        let mut new_tracker = match bincode::deserialize(&encoded) {
            Ok(l) => l,
            Err(why) => {
                LOG.line(LogLevel::Error, why, false);
                SignalTracker(vec![])
            }
        };

        if archive {
            let raw_sig_trackers = archive::get_archived_signal_trackers();
            for raw_sig_tracker in &raw_sig_trackers {
                if let Ok(arc_tracker) = bincode::deserialize::<Vec<SignalTrackerEntry>>(raw_sig_tracker) {
                    for entry in &arc_tracker {
                        new_tracker.0.push(entry.clone());
                    }
                }
            }
            let mut uniques = HashSet::new();
            new_tracker.0.retain(|e| uniques.insert((e.date, e.time)));
        }
        new_tracker.0.sort_unstable();
        new_tracker
    }

    pub fn add_entry(&mut self, entry: SignalTrackerEntry) {
        self.0.push(entry);
        self.save();
    }

    pub fn get_formatted_entries(&self, date: (u32, u32, u32)) -> (StyledString, StyledString, StyledString) {
        if date == (1970, 1, 1) {
            return (StyledString::from(HEADER), StyledString::from(""), StyledString::from(TAIL))
        }
        let mut entries = Vec::new();
        for entry in self.iter_entries_by_date(date) {
            let mut out_str = StyledString::default();
            out_str.append(format!("│{:02}:{:02}│", entry.time.0, entry.time.1));
            out_str.append(if entry.conn_stat { "Yes       │" } else { "No        │" });
            out_str.append(entry.info_4g.fmt_colored());
            out_str.append("│");
            out_str.append(entry.info_5g.fmt_colored());
            out_str.append(if entry.reboot { "│Yes      │\n" } else { "│No       │\n" });
            entries.push(out_str);
            entries.push(StyledString::plain(DIV));
        }

        entries.pop();
        let mut out = StyledString::new();
        for entry in entries {
            out.append(entry);
        }

        (StyledString::from(HEADER), out, StyledString::from(TAIL))
    }

    pub fn get_dates(&self) -> Vec<(u32, u32, u32)> {
        let mut dates = Vec::new();
        for entry in self.0.iter().rev() {
            dates.push(entry.date);
        }
        let mut uniques = HashSet::new();
        dates.retain(|d| uniques.insert(*d));
        dates.sort_unstable();
        dates.reverse();
        dates
    }

    // returns an iterator with the entries of a specific date
    // meant to be used in for loops
    pub fn iter_entries_by_date(&self, date: (u32, u32, u32)) -> impl Iterator<Item = &SignalTrackerEntry> {
        self.0.iter().filter(move |entry| entry.date == date)
    }

    pub fn num_entries(&self, date: (u32, u32, u32)) -> usize {
        self.iter_entries_by_date(date).count()
    }

    pub fn clear(&mut self) {
        self.0 = vec![];
        self.save();
    }

    fn save(&self) {
        let encoded = bincode::serialize(self).unwrap();
        fs::create_dir_all(&*LOG_DIR).expect("Unable to save log file!");
        fs::write(format!("{}/signal_tracker", &*LOG_DIR), &encoded).expect("Unable to save log file!");
    }
}

// entry in the signal tracker
// hopefully this will be able to help
// me find out what the hell is going
// on with this thing's connectivity!
#[derive(Serialize, Deserialize, Clone, Eq)]
pub struct SignalTrackerEntry {
    // the date the signal info was collected at
    // (year, month, day)
    pub date: (u32, u32, u32),

    // the time the signal info was collected at
    // (hour, minute)
    pub time: (u32, u32),

    // is the gateway claiming that it has connectivity?
    pub conn_stat: bool,

    // cell signal info
    pub info_4g: CellInfo,
    pub info_5g: CellInfo,

    // did the gateway reboot?
    pub reboot: bool
}

impl PartialEq for SignalTrackerEntry {
    fn eq(&self, other: &SignalTrackerEntry) -> bool {
        self.date == other.date && self.time == other.time
    }
}

impl Ord for SignalTrackerEntry {
    fn cmp(&self, other: &Self) -> Ordering {
        let stamp = (self.date.0, self.date.1, self.date.2, self.time.0, self.time.1);
        let other_stamp = (other.date.0, other.date.1, other.date.2, other.time.0, other.time.1);

        stamp.cmp(&other_stamp)
    }
}

impl PartialOrd for SignalTrackerEntry {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}