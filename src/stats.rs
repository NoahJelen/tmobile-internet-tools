use std::{fmt, cmp::PartialEq};
use serde_derive::{Serialize, Deserialize};
use cursive::{
    utils::markup::StyledString,
    theme::{ColorStyle, BaseColor, Color}
};
use pad::*;
use crate::devices::DeviceList;

#[derive(Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct GatewayStats {
    pub conn_status: bool,
    pub info_5g: CellInfo,
    pub info_4g: CellInfo,
    pub devices: DeviceList,
    pub networks: Vec<NetworkInfo>,

    #[cfg(feature = "nokia")]
    pub download: u128,

    #[cfg(feature = "nokia")]
    pub upload: u128
}

#[derive(Clone, Serialize, Deserialize)]
pub struct AdvancedGatewayStats {
    // APN info
    pub apn_name: String,
    pub apn_ip4: String,
    pub apn_ip6: String,

    // 5G cell info
    pub band_5g: String,
    pub rsrp_5g: i32,
    pub snr_5g: i32,
    pub rsrq_5g: i32,
    pub rssi: i32,

    // 4G cell info
    pub band_4g: String,
    pub rsrp_4g: i32,
    pub snr_4g: i32,
    pub rsrq_4g: i32,

    // SIM info
    pub imsi: u64,
    pub iccid: u64,
    pub imei: u64
}

#[derive(Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct NetworkInfo {
    pub id: String,
    pub name: String,
    pub enabled: bool,

    #[cfg(feature = "sagemcom")]
    pub password: String,

    #[cfg(feature = "sagemcom")]
    pub net_security: String,

    #[cfg(feature = "sagemcom")]
    pub freq2_4g: bool,

    #[cfg(feature = "sagemcom")]
    pub freq5g: bool,

    #[cfg(feature = "nokia")]
    pub download: u128,

    #[cfg(feature = "nokia")]
    pub upload: u128
}

#[derive(Serialize, Deserialize, Clone, Eq, PartialEq)]
pub struct CellInfo {
    pub band: String,
    pub strength: i32,
    pub bars: u8
}

impl CellInfo {
    pub fn fmt_colored(&self) -> StyledString {
        let mut out_str = StyledString::plain(format!("Band: {} Strength: {} dBm ", self.band, self.strength).with_exact_width(29));
        out_str.append_styled(format!("{: <5}", self.get_bars_vis()), ColorStyle::from((Color::Light(BaseColor::Magenta), Color::TerminalDefault)));
        out_str.append(" ".repeat(34 - out_str.source().chars().count()));
        out_str
    }

    pub fn get_bars_vis(&self) -> &str {
        match self.bars {
            1 => "▁",
            2 => "▁▂",
            3 => "▁▂▄",
            4 => "▁▂▄▆",
            5 => "▁▂▄▆█",
            _ => ""
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct GatewayInfo {
    pub vendor: String,
    pub ser_num: String,
    pub hw_ver: String,
    pub sw_ver: String,
    pub uptime: usize,
    pub imei: u64,

    #[cfg(feature = "sagemcom")]
    pub line_num: String
}

impl fmt::Display for GatewayInfo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let ut_secs = self.uptime % 60;
        let ut_mins = (self.uptime - (self.uptime % 60)) / 60;
        let ut_hrs = ut_mins / 60;
        writeln!(f, "Vendor: {}", self.vendor)?;
        writeln!(f, "Serial Number: {}", self.ser_num)?;
        writeln!(f, "Hardware Version: {}", self.hw_ver)?;
        writeln!(f, "Software Version: {}", self.sw_ver)?;
        writeln!(f, "Uptime: {ut_hrs}:{:02}:{ut_secs:02}", ut_mins % 60)?;

        #[cfg(feature = "sagemcom")]
        {
            writeln!(f, "Gateway IMEI: {}", self.imei)?;
            writeln!(f, "Line Number: {}", self.line_num)
        }

        #[cfg(not(feature = "sagemcom"))]
        writeln!(f, "Gateway IMEI: {}", self.imei)
    }
}

// take a byte measurement and convert to the smallest possible unit
#[cfg(feature = "nokia")]
pub fn format_bytes(bytes: u128) -> String {
    if calc_kilos(bytes) < 1.0 { format!("{bytes}") }
    else if calc_megs(bytes) < 1.0 { format!("{:.2} KB", calc_kilos(bytes)) }
    else if calc_gigs(bytes) < 1.0 { format!("{:.2} MB", calc_megs(bytes)) }
    else { format!("{:.2} GB", calc_gigs(bytes)) }
}

#[cfg(feature = "nokia")]
fn calc_kilos(bytes: u128) -> f64 { bytes as f64 / 1024. }

#[cfg(feature = "nokia")]
fn calc_megs(bytes: u128) -> f64 { bytes as f64 / 1048576. }

#[cfg(feature = "nokia")]
fn calc_gigs(bytes: u128) -> f64 { bytes as f64 / 1073741824. }