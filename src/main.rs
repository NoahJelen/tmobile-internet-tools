use tmo_tools::*;
use rust_utils::utils;

mod gatewaymon;
mod tmotop;
mod cli;

fn main() {
    LOG.report_panics(true);
    match utils::get_execname().as_str() {
        "gatewaymon" => gatewaymon::init(),
        "tmotop" => tmotop::init(),
        "tmocli" => cli::parse_args(),
        _ => unreachable!()
    }
}