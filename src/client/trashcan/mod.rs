use super::{
    ClientError,
    ClientResult,
    GatewayClient,
    JSONUnwrappable,
    parse_json
};
use std::{
    sync::Arc,
    time::Duration
};
use json::JsonValue;
use reqwest::{
    blocking::Client,
    cookie::Jar
};
use crate::{
    devices::DeviceList,
    config::{DeviceConfig, GlobalConfig, Config},
    stats::{AdvancedGatewayStats, CellInfo, GatewayInfo, NetworkInfo},
    client::GatewayModel
};

mod token;
use token::AuthToken;

pub struct TrashcanClient {
    client: Client,
    jar: Arc<Jar>,
}

#[allow(clippy::new_without_default)]
impl TrashcanClient {
    // create a new client
    pub fn new() -> TrashcanClient {
        let jar = Arc::new(Jar::default());
        let client = Client::builder()
            .timeout(Duration::from_secs(10))
            .cookie_store(true)
            .cookie_provider(jar.clone())
            .build()
            .unwrap();
        TrashcanClient {
            client,
            jar
        }
    }

    // POST request to the gateway
    fn post(&self, res: &str) -> ClientResult<()> {
        let config = GlobalConfig::load();
        let addr = format!("http://{}/{res}", config.gateway_ip);
        let req = self.client.post(addr);
        let token = self.gen_token()?;
        match req.form(&token.get_data()).send() {
            Ok(_) => Ok(()),
            Err(why) => Err(ClientError::GatewayUnreachable(why.to_string()))
        }
    }

    // GET request to the gateway (returns JSON)
    fn json_get(&self, res: &str, auth: bool) -> ClientResult<JsonValue> {
        let config = GlobalConfig::load();
        let addr = format!("http://{}/{res}", config.gateway_ip);
        let req = self.client.get(addr);
        if auth { let _token = self.gen_token()?; }

        match req.send() {
            Ok(response) => {
                let text = response.text().unwrap();
                Ok(parse_json(&text)?)
            },
            Err(why) => Err(ClientError::GatewayUnreachable(why.to_string()))
        }
    }

    fn gen_token(&self) -> ClientResult<AuthToken> {
        let config = GlobalConfig::load();
        let token_r = AuthToken::new(&self.client, &config.username, &config.password);
        if let Ok(token) = token_r {
            token.put_cookies_in_jar(&self.jar);
            Ok(token)
        }
        else {
            token_r
        }
    }
}

impl GatewayClient for TrashcanClient {
    // request the gateway to reboot (no log output)
    fn reboot_gateway_no_log(&self) -> Result<(), ClientError> { self.post("reboot_web_app.cgi") }

    // get a list of devices and their ipv4 addresses connected to the gateway
    fn get_devices(&self) -> ClientResult<DeviceList> {
        let json = self.json_get("dashboard_device_info_status_web_app.cgi", false)?;
        let dev_config = DeviceConfig::load();
        DeviceList::get_nokia(json, &dev_config)
    }

    // get the 5G metrics
    fn get_5g_stats(&self) -> ClientResult<CellInfo> {
        let json = self.json_get("fastmile_radio_status_web_app.cgi", false)?;
        let bars = json["cell_LTE_stats_cfg"][0]["stat"]["RSRPStrengthIndexCurrent"].as_u8().unwrap_json("RSRPStrengthIndexCurrent", &json)?;
        let strength = json["cell_LTE_stats_cfg"][0]["stat"]["RSRPCurrent"].as_i32().unwrap_json("RSRPCurrent", &json)?;
        let band = json["cell_5G_stats_cfg"][0]["stat"]["Band"].as_str().unwrap_json("Band", &json)?.to_string();

        Ok(CellInfo {
            band,
            bars,
            strength
        })
    }

    // get the LTE metrics
    fn get_4g_stats(&self) -> ClientResult<CellInfo> {
        let json = self.json_get("fastmile_radio_status_web_app.cgi", false)?;
        let bars = json["cell_5G_stats_cfg"][0]["stat"]["RSRPStrengthIndexCurrent"].as_u8().unwrap_json("RSRPStrengthIndexCurrent", &json)?;
        let strength = json["cell_5G_stats_cfg"][0]["stat"]["RSRPCurrent"].as_i32().unwrap_json("RSRPCurrent", &json)?;
        let band = json["cell_LTE_stats_cfg"][0]["stat"]["Band"].as_str().unwrap_json("Band", &json)?.to_string();

        Ok(CellInfo {
            band,
            bars,
            strength
        })
    }

    // get the connection status as a boolean
    fn get_conn_status(&self) -> bool {
        let json_res = self.json_get("fastmile_radio_status_web_app.cgi", false);
        if let Ok(json) = json_res {
            let val = json["connection_status"][0]["ConnectionStatus"].as_usize();
            return if let Some(v) = val { v == 1 } else { false }
        }
        false
    }

    // get info about the gateway
    fn get_info(&self) -> ClientResult<GatewayInfo> {
        let json = self.json_get("device_status_web_app.cgi", true)?;
        let sim_json = self.json_get("fastmile_statistics_status_web_app.cgi", true)?;
        let vendor = json["Vendor"].as_str().unwrap_json("Vendor", &json)?.to_string();
        let ser_num = json["SerialNumber"].as_str().unwrap_json("SerialNumber", &json)?.to_string();
        let hw_ver = json["HardwareVersion"].as_str().unwrap_json("HardwareVersion", &json)?.to_string();
        let sw_ver = json["SoftwareVersion"].as_str().unwrap_json("SoftwareVersion", &json)?.to_string();
        let uptime = json["UpTime"].as_usize().unwrap_json("UpTime", &json)?;
        let imei = sim_json["network_cfg"][0]["IMEI"].as_str().unwrap_json("IMEI", &sim_json)?.parse().unwrap();
        Ok(GatewayInfo {
            vendor,
            ser_num,
            hw_ver,
            sw_ver,
            uptime,
            imei,

            #[cfg(feature = "sagemcom")]
            line_num: "N/A".to_string()
        })
    }

    // get advanced gateway stats
    fn get_adv_stats(&self) -> ClientResult<AdvancedGatewayStats> {
        let json = self.json_get("fastmile_radio_status_web_app.cgi", false)?;

        // APN info
        let apn_name = json["apn_cfg"][0]["APN"].as_str().unwrap().to_string();
        let apn_ip4 = json["apn_cfg"][0]["X_ALU_COM_IPAddressV4"].as_str().unwrap_json("X_ALU_COM_IPAddressV4", &json)?.to_string();
        let apn_ip6 = json["apn_cfg"][0]["X_ALU_COM_IPAddressV6"].as_str().unwrap_json("X_ALU_COM_IPAddressV6", &json)?.to_string();

        // 5G cell info
        let band_5g = json["cell_5G_stats_cfg"][0]["stat"]["Band"].as_str().unwrap_json("Band", &json)?.to_string();
        let rsrp_5g = json["cell_LTE_stats_cfg"][0]["stat"]["RSRPCurrent"].as_i32().unwrap_json("RSRPCurrent", &json)?;
        let snr_5g = json["cell_5G_stats_cfg"][0]["stat"]["SNRCurrent"].as_i32().unwrap_json("SNRCurrent", &json)?;
        let rsrq_5g = json["cell_LTE_stats_cfg"][0]["stat"]["RSRQCurrent"].as_i32().unwrap_json("RSRQCurrent", &json)?;
        let rssi = json["cell_LTE_stats_cfg"][0]["stat"]["RSSICurrent"].as_i32().unwrap_json("RSSICurrent", &json)?;

        // 4G cell info
        let band_4g = json["cell_LTE_stats_cfg"][0]["stat"]["Band"].as_str().unwrap_json("cell_LTE_stats_cfg", &json)?.to_string();
        let rsrp_4g = json["cell_5G_stats_cfg"][0]["stat"]["RSRPCurrent"].as_i32().unwrap_json("cell_5G_stats_cfg", &json)?;
        let snr_4g = json["cell_LTE_stats_cfg"][0]["stat"]["SNRCurrent"].as_i32().unwrap_json("cell_LTE_stats_cfg", &json)?;
        let rsrq_4g = json["cell_5G_stats_cfg"][0]["stat"]["RSRQCurrent"].as_i32().unwrap_json("cell_5G_stats_cfg", &json)?;

        // SIM info
        let sim_json = self.json_get("fastmile_statistics_status_web_app.cgi", true)?;
        let imsi = sim_json["sim_cfg"][0]["IMSI"].as_str().unwrap_json("IMSI", &sim_json)?.parse().unwrap();
        let iccid = sim_json["sim_cfg"][0]["ICCID"].as_str().unwrap_json("ICCID", &sim_json)?.parse().unwrap();
        let imei = sim_json["network_cfg"][0]["IMEI"].as_str().unwrap_json("IMEI", &sim_json)?.parse().unwrap();

        Ok(AdvancedGatewayStats {
            apn_name,
            apn_ip4,
            apn_ip6,
            band_5g,
            rsrp_5g,
            snr_5g,
            rsrq_5g,
            rssi,
            band_4g,
            rsrp_4g,
            snr_4g,
            rsrq_4g,
            imsi,
            iccid,
            imei
        })
    }

    // get download data usage
    fn get_download(&self) -> ClientResult<u128> {
        let json = self.json_get("fastmile_radio_status_web_app.cgi", false)?;
        Ok(json["cellular_stats"][0]["BytesReceived"].as_u64().unwrap_json("BytesReceived", &json)? as u128)
    }

    // get upload data usage
    fn get_upload(&self) -> ClientResult<u128> {
        let json = self.json_get("fastmile_radio_status_web_app.cgi", false)?;
        Ok(json["cellular_stats"][0]["BytesSent"].as_u64().unwrap_json("BytesSent", &json)? as u128)
    }

    // get list of active networks
    fn  get_networks(&self) -> ClientResult<Vec<NetworkInfo>> {
        let json = self.json_get("statistics_status_web_app.cgi", false)?;
        let eth_networks = json["LAN"].members();
        let mut networks: Vec<NetworkInfo> = vec![];
        for (i, network_raw) in eth_networks.enumerate() {
            let id = network_raw["Name"].as_str().unwrap_json("Name", network_raw)?.to_string();
            let name = format!("Ethernet {}", i + 1);
            let upload = network_raw["BytesSent"].as_u64().unwrap_json("BytesSent", network_raw)? as u128;
            let download = network_raw["BytesReceived"].as_u64().unwrap_json("BytesReceived", network_raw)? as u128;
            let enable_str = &network_raw["Enable"].as_u32().unwrap_json("Enable", network_raw)?;

            networks.push(NetworkInfo {
                id,
                name,
                upload,
                download,
                enabled: (*enable_str == 1),

                #[cfg(feature = "sagemcom")]
                password: String::new(),

                #[cfg(feature = "sagemcom")]
                net_security: String::new(),

                #[cfg(feature = "sagemcom")]
                freq2_4g: false,

                #[cfg(feature = "sagemcom")]
                freq5g: false
            })
        }

        let wifi_networks = json["WLAN"].members();

        for network_raw in wifi_networks {
            let id = network_raw["Name"].as_str().unwrap_json("Name", network_raw)?.to_string();
            let name = network_raw["SSID"].as_str().unwrap_json("SSID", network_raw)?.to_string();
            let upload = network_raw["BytesSent"].as_u64().unwrap_json("BytesSent", network_raw)? as u128;
            let download = network_raw["BytesReceived"].as_u64().unwrap_json("BytesReceived", network_raw)? as u128;
            let enable = &network_raw["Enable"].as_u32().unwrap_json("Enable", network_raw)?;

            networks.push(NetworkInfo {
                id,
                name,
                upload,
                download,
                enabled: (*enable == 1),

                #[cfg(feature = "sagemcom")]
                password: String::new(),

                #[cfg(feature = "sagemcom")]
                net_security: String::new(),

                #[cfg(feature = "sagemcom")]
                freq2_4g: false,

                #[cfg(feature = "sagemcom")]
                freq5g: false
            })
        }

        Ok(networks)
    }

    // gateway picture for the web interface
    fn webui_pic(&self) -> &'static str { "trashcan" }

    // get the model of the gateway
    fn model(&self) -> GatewayModel { GatewayModel::Nokia }
}