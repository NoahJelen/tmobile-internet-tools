use super::*;
use std::{fmt::Write, collections::HashSet};
use cfg_if::cfg_if;
use cursive::{
    direction,
    event::{MouseEvent, MouseButton},
    view::{CannotFocus, View},
    views::{Button, HideableView},
    Printer,
    Vec2,
    event::EventResult
};
use crate::config::{DeviceConfig, Config};

#[cfg(feature = "sagemcom")]
use qrcode::render::unicode;

#[cfg(feature = "nokia")]
use cursive::{
    views::Canvas,
    utils::{
        span::SpannedStr,
        markup::StyledString
    }
};

#[cfg(feature = "nokia")]
pub struct DataUsageView {
    width: u32,
    height: u32,
    download: u128,
    upload: u128,
}

#[cfg(feature = "nokia")]
impl DataUsageView {
    pub fn new(download: u128, upload: u128) -> DataUsageView {
        DataUsageView {
            width: 0,
            height: 0,
            download,
            upload
        }
    }

    pub fn update(&mut self, download: u128, upload: u128) {
        self.download = download;
        self.upload = upload;
    }
}

#[cfg(feature = "nokia")]
impl View for DataUsageView {
    fn draw(&self, printer: &Printer) {
        if self.width == 0 || self.height == 0 {
            return;
        }

        let total_bytes = self.upload + self.download;
        let download_str = format!("Download: {}", format_bytes(self.download));
        let upload_str = format!("Upload: {}", format_bytes(self.upload));
        let total_str = format!("Total: {}", format_bytes(total_bytes));
        printer.with_color(ColorStyle::from(Color::Light(BaseColor::Magenta)), |printer| printer.print((0, 0), &download_str));
        printer.with_color(ColorStyle::from(Color::Light(BaseColor::Cyan)), |printer| printer.print((0, 1), &upload_str));
        printer.print((0, 2), &total_str);
        data_usage_bar(printer, (0, self.height - 4), self.download, self.upload, self.width, 3, false);
        printer.print((0, self.height - 1), "Disclaimer: This is the data usage since the last restart")
    }

    fn required_size(&mut self, constraint: Vec2) -> Vec2 {
        self.width = constraint.x as u32;
        self.height = constraint.y as u32;
        constraint
    }
}

pub struct CellStatusView {
    width: u32,
    height: u32,
    info_5g: CellInfo,
    info_4g: CellInfo
}

impl CellStatusView {
    pub fn new(info_4g: &CellInfo, info_5g: &CellInfo) -> CellStatusView {
        CellStatusView {
            width: 0,
            height: 0,
            info_4g: info_4g.clone(),
            info_5g: info_5g.clone()
        }
    }

    pub fn update(&mut self, info_4g: &CellInfo, info_5g: &CellInfo) {
        self.info_4g = info_4g.clone();
        self.info_5g = info_5g.clone();
    }
}

impl View for CellStatusView {
    fn draw(&self, printer: &Printer) {
        let middle_h = self.height / 2 + 2;
        let middle_v = self.width / 2 - 18;

        if self.width == 0 || self.height == 0 {
            return;
        }
        let bar_style = ColorStyle::new(
            Color::Light(BaseColor::Magenta),
            Color::Light(BaseColor::Magenta)
        );

        printer.print((0, 0), "5G:");
        printer.print((2, 1), &format!("Band: {}", self.info_5g.band));
        printer.print((2, 2), &format!("Strength: {} dBm", self.info_5g.strength));
        printer.print((25, 0), "4G (LTE):");
        printer.print((26, 1), &format!("Band: {}", self.info_4g.band));
        printer.print((26, 2), &format!("Strength: {} dBm", self.info_4g.strength));
        
        for x in 1..=self.info_4g.bars as u32 {
            for y in 0..x {
                printer.with_color(bar_style, |printer| {
                    printer.print((x * 3 + middle_v, middle_h - y), " ");
                    printer.print((x * 3 + middle_v + 1, middle_h - y), " ");
                });
            }
        }

        for x in 1..=self.info_5g.bars as u32 {
            for y in 0..x {
                printer.with_color(bar_style, |printer| {
                    printer.print((x * 3 + middle_v + 18, middle_h - y), " ");
                    printer.print((x * 3 + middle_v + 19, middle_h - y), " ");
                });
            }
        }

        printer.print((middle_v + 9, middle_h + 2), "LTE");
        printer.print((middle_v + 27, middle_h + 2), "5G");
    }

    fn required_size(&mut self, constraint: Vec2) -> Vec2 {
        self.width = constraint.x as u32;
        self.height = constraint.y as u32;
        constraint
    }
}

pub struct NetworkView {
    selected: bool,
    width: u32,
    network: NetworkInfo
}

impl NetworkView {
    pub fn new(network: NetworkInfo) -> NetworkView {
        NetworkView {
            width: 0,
            selected: false,
            network
        }
    }

    #[cfg(feature = "nokia")]
    fn get_data_usage(&self) -> StyledString {
        if CLIENT.model() == GatewayModel::Nokia {
            let mut new_str = StyledString::new();
            let download_style = ColorStyle::from(Color::Light(BaseColor::Magenta));
            let upload_style = ColorStyle::from(Color::Light(BaseColor::Cyan));
            new_str.append_styled(format!("Down: {}", format_bytes(self.network.download)), download_style);
            new_str.append_plain(" ");
            new_str.append_styled(format!("Up: {}", format_bytes(self.network.upload)), upload_style);
            return new_str;
        }

        StyledString::new()
    }
}

impl View for NetworkView {
    fn draw(&self, printer: &Printer) {
        let back_color = if self.selected { Color::Dark(BaseColor::White) }
        else { Color::TerminalDefault };
        let default_style = ColorStyle::new(Color::Dark(if self.selected { BaseColor::Black } else { BaseColor::White }), back_color);
        let title_style = ColorStyle::new(if self.selected { Color::Dark(BaseColor::Black)} else { Color::Light(BaseColor::White)}, back_color);
        for x in 0..self.width {
            for y in 0..3 {
                printer.with_color(default_style, |printer| printer.print((x, y), " "));
            }
        }

        printer.with_color(title_style, |printer| printer.print((0, 0), &self.network.name));
        printer.with_color(default_style, |printer| {
            #[cfg(feature = "nokia")]
            {
                let data = self.get_data_usage();
                let data_str = SpannedStr::new(data.source(), data.spans_raw());
                printer.print_styled((0, 1), data_str);
            }

            #[cfg(feature = "sagemcom")]
            {
                let bands = {
                    let mut base = String::from("Bands: ");
                    if self.network.freq2_4g {
                        write!(base, "2.4GHz ").unwrap();
                    }

                    if self.network.freq5g {
                        write!(base, "5GHz ").unwrap();
                    }
                    base
                };
                printer.print((0, 1), &format!("Security: {} {bands}", self.network.net_security));
            }
        });

        #[cfg(feature = "nokia")]
        {
            if CLIENT.model() == GatewayModel::Nokia {
                data_usage_bar(printer, (0, 2), self.network.download, self.network.upload, self.width, 1, self.selected);
            }
        }
    }

    fn required_size(&mut self, constraint: Vec2) -> Vec2 {
        self.width = constraint.x as u32;
        cfg_if! {
            if #[cfg(feature = "nokia")] { Vec2::new(constraint.x, 3) }
            else { Vec2::new(constraint.x, 2) }
        }
    }

    fn on_event(&mut self, event: Event) -> EventResult {
        match event {
            Event::FocusLost => {
                self.selected = false;
                EventResult::consumed()
            },

            Event::Key(Key::Enter) | Event::Mouse { event: MouseEvent::Press(MouseButton::Left), .. } => {
                let name = self.network.name.clone();
                let id = self.network.id.clone();
                #[cfg(feature = "sagemcom")]
                let password = self.network.password.clone();

                #[cfg(feature = "sagemcom")]
                let security = self.network.net_security.clone();

                #[cfg(feature = "sagemcom")]
                let bands = {
                    let mut base = String::from("Bands: ");
                    if self.network.freq2_4g {
                        write!(base, "2.4GHz ").unwrap();
                    }

                    if self.network.freq5g {
                        write!(base, "5GHz ").unwrap();
                    }
                    base
                };

                cfg_if! {
                    if #[cfg(feature = "nokia")] {
                        let download = self.network.download;
                        let upload = self.network.upload;
                        let data = self.get_data_usage();
                        let data_u = Canvas::new(data)
                            .with_draw(move |data: &StyledString, printer| {
                                let text = SpannedStr::new(data.source(), data.spans_raw());
                                printer.print_styled((0, 0), text);
                                let length = (data.source().len() * 2) as u32;
                                data_usage_bar(printer, (0, 1), download, upload, length, 1, false);
                            })
                            .with_required_size(|data: &mut StyledString, _| (data.source().len() * 2, 2).into());
                    }
                }

                EventResult::with_cb_once(move |view| {
                    let text = if CLIENT.model() == GatewayModel::Nokia {
                        cfg_if! {
                            if #[cfg(feature = "nokia")] {
                                vlayout!(
                                    TextView::new(format!("Network Name: {name}\nNetwork ID: {id}\n\nData Usage:")),
                                    data_u
                                )
                            }
                            else { vlayout!(TextView::new(format!("Network Name: {name}\nNetwork ID: {id}"))) }
                        }
                    }
                    else if CLIENT.model() == GatewayModel::Sagemcom {
                        cfg_if! {
                            if #[cfg(feature = "sagemcom")] {
                                vlayout!(
                                    TextView::new(format!("Network Name: {name}\nPassword: {password}\nSecurity: {security}\n{bands}\nNetwork ID: {id}")),
                                    Button::new("Show Wifi QR Code", |view| {
                                        let mut net_code = view.find_name::<HideableView<TextView>>("net_code").unwrap();
                                        let mut net_code_btn = view.find_name::<Button>("net_code_btn").unwrap();
                                        let visible = net_code.is_visible();
                                        net_code.set_visible(!visible);
                                        if net_code.is_visible() {
                                            net_code_btn.set_label("Hide Wifi QR Code");
                                        }
                                        else {
                                            net_code_btn.set_label("Show Wifi QR Code");
                                        }
                                    }).with_name("net_code_btn"),
                                    HideableView::new(gen_net_qr_code(&name, &password, &security)).hidden().with_name("net_code")
                                )
                            }
                            else { vlayout!(TextView::new(format!("Network Name: {name}\nNetwork ID: {id}"))) }
                        }
                    }
                    else { vlayout!(TextView::new(format!("Network Name: {name}\nNetwork ID: {id}"))) };

                    let layout = Dialog::around(text)
                        .dismiss_button("Back")
                        .title("Network Info")
                        .wrap_with(OnEventView::new)
                        .on_event(Event::Key(Key::Esc), |v| { v.pop_layer(); });

                    view.add_layer(layout);
                })
            },

            _ => EventResult::Ignored
        }
    }

    fn take_focus(&mut self, _: direction::Direction) -> Result<EventResult, CannotFocus> {
        self.selected = true;
        Ok(EventResult::consumed())
    }
}

pub struct ConnectedDeviceView {
    selected: bool,
    width: usize,
    device: Device
}

impl ConnectedDeviceView {
    pub fn new(device: Device) -> ConnectedDeviceView {
        ConnectedDeviceView {
            width: 0,
            selected: false,
            device
        }
    }
}

impl View for ConnectedDeviceView {
    fn draw(&self, printer: &Printer) {
        let back_color = if self.selected {
            Color::Dark(BaseColor::White)
        }
        else {
            Color::TerminalDefault
        };
        let name_color = if self.device.active {
            Color::Light(BaseColor::Magenta)
        }
        else {
            Color::Dark(BaseColor::White)
        };

        let default_style = ColorStyle::new(Color::Dark(if self.selected { BaseColor::Black } else { BaseColor::White }), back_color);

        for x in 0..self.width {
            for y in 0..2 {
                printer.with_color(default_style, |printer| printer.print((x, y), " "));            
            }
        }

        printer.with_color(ColorStyle::new(name_color, back_color), |printer| printer.print((0, 0), self.device.get_name()));
        printer.with_color(default_style, |printer| printer.print((0, 1), &self.device.get_ip()));
    }

    fn required_size(&mut self, constraint: Vec2) -> Vec2 {
        self.width = constraint.x;
        Vec2::new(constraint.x, 2)
    }

    fn on_event(&mut self, event: Event) -> EventResult {
        match event {
            Event::FocusLost => {
                self.selected = false;
                EventResult::consumed()
            },

            Event::Key(Key::Enter) | Event::Mouse { event: MouseEvent::Press(MouseButton::Left), .. } => {
                let dev_name = self.device.get_name().to_string();
                let dev_real_name = self.device.get_real_name().to_string();
                let is_gateway = self.device.is_gateway();
                let dev_ip = self.device.get_ip();
                let dev_mac_addr = self.device.get_mac_addr();

                EventResult::with_cb_once(move |view| {
                    let content = format!("Device Name (alias): {dev_name}\nDevice IP address: {dev_ip}{}\n\nSet new alias:", if is_gateway{ String::new() } else { format!("\nDevice MAC address: {dev_mac_addr}") });
                    let layout = Dialog::around(
                        vlayout!(
                            TextView::new(content),
                            styled_editview_color(&dev_name, "alias", false, Color::Dark(BaseColor::Magenta)),
                            Button::new("Set alias", move |view| {
                                let dev_name2 = dev_name.clone();
                                let alias_view = view.find_name::<EditView>("alias").unwrap();
                                let alias = alias_view.get_content().to_string();

                                load_resource(view, "Please wait...", "Saving Settings...",
                                    move || {
                                        let mut hst_config = DeviceConfig::load();
                                        hst_config.aliases.insert(dev_name2.to_string(), alias);
                                        hst_config.save().expect("Unable to save config!");
                                    },
                                    |_, _: ()| { }
                                );
                            }),
                            Button::new("Reset alias", move |view| {
                                let dev_real_name2 = dev_real_name.clone();
                                load_resource(view, "Please wait...", "Saving Settings...",
                                    move || {
                                        let mut hst_config = DeviceConfig::load();
                                        hst_config.aliases.remove(&dev_real_name2);
                                        hst_config.save().expect("Unable to save config!");
                                    },
                                    |_, _: ()| { }
                                );
                            })
                        )
                    )
                        .dismiss_button("Back")
                        .title("Device Info")
                        .wrap_with(OnEventView::new)
                        .on_event(Event::Key(Key::Esc), |v| { v.pop_layer(); });

                    view.add_layer(layout);
                })
            },

            _ => EventResult::Ignored
        }
    }

    fn take_focus(&mut self, _: direction::Direction) -> Result<EventResult, CannotFocus> {
        self.selected = true;
        Ok(EventResult::consumed())
    }
}

pub struct ConnStatVisual {
    // 0: 5G signal
    // 1: LTE (4G) signal
    // 2: hour
    // 3: did the gateway have to reboot?
    data: Vec<(i32, i32, u32, bool)>
}

impl ConnStatVisual {
    pub fn new(data: Vec<(i32, i32, u32, bool)>) -> ConnStatVisual {
        ConnStatVisual {
            data
        }
    }

    pub fn get_info_view() -> TextView {
        TextView::new("           -70│\n              │\n              │\n           -85│\n              │\n              │\n Strength -100│\n   (dBm)      │\n              │\n          -115│\n              │\n              │\n          -130│\n              └\n        Time:")
    }
}

impl View for ConnStatVisual {
    fn draw(&self, printer: &Printer) {
        let mut times = HashSet::new();
        for (i, entry) in self.data.iter().enumerate() {
            vis_bar(printer, i, *entry, times.insert(entry.2));
        }
    }

    fn required_size(&mut self, _: Vec2) -> Vec2 { Vec2::new(self.data.len(), 15) }
}

fn vis_bar(printer: &Printer, x: usize, entry: (i32, i32, u32, bool), time: bool) {
    let style_4g = if entry.3 {
        ColorStyle::new(Color::TerminalDefault, Color::Dark(BaseColor::Red))
    }
    else {
        ColorStyle::new(Color::TerminalDefault, Color::Dark(BaseColor::Cyan))
    };

    let style_5g = if entry.3 {
        ColorStyle::new(Color::TerminalDefault, Color::Light(BaseColor::Red))
    }
    else {
        ColorStyle::new(Color::TerminalDefault, Color::Light(BaseColor::Cyan))
    };

    let height_5g = (((-entry.0) - 70) / 5) as u32;
    let height_4g = (((-entry.1) - 70) / 5) as u32;
    let print_bar = |height, style| {
        for y in height..13 {
            printer.with_color(style, |printer| {
                printer.print((x as u32, y), " ");
            });
        }
    };

    if height_5g < height_4g {
        print_bar(height_5g, style_5g);
        print_bar(height_4g, style_4g);
    }
    else {
        print_bar(height_4g, style_4g);
        print_bar(height_5g, style_5g);
    }

    printer.print((x, 13), "─");

    if time {
        printer.print((x, 14), &format!("{}", entry.2));
    }
}

#[cfg(feature = "sagemcom")]
fn gen_net_qr_code(name: &str, password: &str, security: &str) -> TextView {
    let qr_code = client::net_qr_code(name, password, security);
    let qr_string = qr_code.render::<unicode::Dense1x2>()
        .dark_color(unicode::Dense1x2::Light)
        .light_color(unicode::Dense1x2::Dark)
        .build();

    TextView::new(qr_string)
}

#[cfg(feature = "nokia")]
fn data_usage_bar(printer: &Printer, loc: (u32, u32), download: u128, upload: u128, width: u32, height: u32, selected: bool) {
    let download_color = if selected { Color::Light(BaseColor::Magenta) }
    else { Color::Dark(BaseColor::Magenta) };

    let upload_color = if selected { Color::Light(BaseColor::Cyan) }
    else { Color::Dark(BaseColor::Cyan) };

    let download_style = ColorStyle::new(download_color, download_color);
    let upload_style = ColorStyle::new(upload_color, upload_color);
    if upload > 0 && download > 0 {
        let total_bytes = upload + download;
        let download_per = download as f64 / total_bytes as f64;
        let download_por = (width as f64 * download_per).ceil() as u32 - 1;

        for x in loc.0..width + loc.0 {
            for y in loc.1..height + loc.1 {
                if x <= download_por {
                    printer.with_color(download_style, |printer| {
                        printer.print((x, y), " ");
                    });
                }
                else {
                    printer.with_color(upload_style, |printer| {
                        printer.print((x, y), " ");
                    });
                }
            }
        }
    }
    else {
        for x in loc.0..width + loc.0 {
            for y in loc.1..height + loc.1 {
                printer.with_color(download_style, |printer| {
                    printer.print((x, y), " ");
                });
            }
        }
    }
}