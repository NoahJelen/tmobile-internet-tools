use std::{
    sync::RwLock,
    thread, env, process,
    time::Duration
};
use cfg_if::cfg_if;
use cursive::{
    Cursive,
    menu, traits::*,
    event::{Event, Key},
    theme::{PaletteColor, Color, BaseColor, ColorStyle},
    views::{
        Dialog,
        OnEventView,
        TextView,
        EditView,
        ViewRef,
        LinearLayout,
        Button
    }
};
use colored::Colorize;
use rust_utils::logging::LogLevel;
use cursive_extras::*;
use crate::{
    client::{self, GatewayModel},
    stats::*,
    devices::*,
    LOG,
    CLIENT,
    VERSION
};
use lazy_static::lazy_static;
use crossbeam_channel::{bounded, Sender, Receiver};

mod views;
mod ui;

use views::*;
use ui::*;

const MESSAGES: [&str; 3] = ["Network seems fine...", "Your gateway needs rebooting!", "Gateway is off or rebooting"];

lazy_static! {
    static ref STATS: RwLock<GatewayStats> = match CLIENT.get_all_stats() {
        Ok(s) => RwLock::new(s),
        Err(why) => {
            let msg = format!("Unable to get gateway stats: {why}").bright_red();
            eprintln!("{msg}");
            process::exit(101);
        }
    };

    static ref CHANNEL: (Sender<()>, Receiver<()>) = bounded(0);
}

pub fn init() {
    let receiver = &CHANNEL.1;
    println!("Collecting gateway statistics...");

    // background thread that updates the stats
    thread::spawn(move || {
        let sender = &CHANNEL.0;
        loop {
            if let Ok(mut stats) = STATS.try_write() {
                let new_stats = match CLIENT.get_all_stats() {
                    Ok(s) => s,
                    Err(why) => {
                        LOG.line(LogLevel::Error, why, false);
                        continue;
                    }
                };
                if *stats != new_stats {
                    *stats = new_stats;
                    sender.send(()).unwrap_or(());
                }
            }
            thread::sleep(Duration::from_secs(10));
        }
    });

    let mut root = cursive::default();

    // menu
    root.set_autohide_menu(false);
    root.add_global_callback(Key::Up, |v| v.select_menubar());
    root.menubar()
        .add_subtree("Settings", menu::Tree::new()
            .leaf("General Settings", general_settings)
            .leaf("Connected Device Settings", device_settings)
            .leaf("Gateway Settings", gateway_settings)
            .leaf("Reboot Gateway", |view| {
                view.add_layer(
                    confirm_dialog("Reboot Gateway", "Are you sure? Your network will be down for about 5 minutes.", |view| { 
                        CLIENT.reboot_gateway(false).unwrap_or_default();
                        view.quit();
                    })
                )
            })
        )
        .add_subtree("Diagnostics", menu::Tree::new()
            .leaf("Check Network for Problems", |view| {
                load_resource(view, "Please wait...", "Checking network...",
                    || MESSAGES[client::check_net_env(true)],
                    |view, msg: &str| {
                        let mut status: ViewRef<StatusView> = view.find_name("status").unwrap();
                        status.info(msg);
                    }
                );
            })
            .leaf("Gateway Monitor Log", gw_monitor_log)
            .leaf("Connection Stats Log", conn_stats_log)
            .leaf("Connection Stats Visual", conn_stats_vis)
        )
        .add_leaf("Gateway Info", |view| {
            load_resource(view, "Please wait...", "Getting gateway info...",
                || CLIENT.get_info(),
                |view, res: Result<GatewayInfo, client::ClientError>| {
                    let mut status: ViewRef<StatusView> = view.find_name("status").unwrap();
                    let info = report_error!(status, res);
                    view.add_layer(info_dialog("Gateway Info", info));
                }
            );
        })
        .add_leaf("Help", |view| {
            let log_path = format!("{}/.local/share/tmo-tools", env::var("HOME").expect("Where the hell is your home folder?!"));
            let cfg_path = format!("{}/.config/tmo-tools", env::var("HOME").expect("Where the hell is your home folder?!"));
            let webui_link = if CLIENT.model() == GatewayModel::Sagemcom { "" } else { "Gateway web interface: http://192.168.12.1\n" };
            let help_text = format!(
                "T-Mobile Home Internet Tools (tmotop)\n\
                Version {VERSION}\n\
                \n\
                Key Shortcuts:\n\
                q: Quit\n\
                F5: Reload all settings\n\
                \n\
                {webui_link}\
                Log file directory: {log_path}\n\
                Settings directory: {cfg_path}"
            );
            view.add_layer(info_dialog("Help", help_text));
        })
        .add_leaf("Advanced Stats", |view| {
            load_resource(view, "Please wait...", "Getting gateway stats...",
                || CLIENT.get_adv_stats(),
                |view, res: client::ClientResult<AdvancedGatewayStats>| {
                    let mut status: ViewRef<StatusView> = view.find_name("status").unwrap();
                    let stats = report_error!(status, res);
                    let stats_text = format!(
                        "APN Info:\n\
                        APN Name: {}\n\
                        IPv4 Address: {}\n\
                        IPv6 Address: {}\n\
                        \n\
                        5G Cell Info:\n\
                        Band: {}\n\
                        RSRP: {} dBm\n\
                        SNR: {} dB\n\
                        RSRQ: {} dB\n\
                        RSSI: {} dBm\n\
                        \n\
                        4G Cell Info:\n\
                        Band: {}\n\
                        RSRP: {} dBm\n\
                        SNR: {} dB\n\
                        RSRQ: {} dB\n\
                        \n\
                        SIM Info:\n\
                        IMSI: {}\n\
                        ICCID: {}\n\
                        Gateway IMEI: {}",
                        stats.apn_name,
                        stats.apn_ip4,
                        stats.apn_ip6,
                        stats.band_5g,
                        stats.rsrp_5g,
                        stats.snr_5g,
                        stats.rsrq_5g,
                        stats.rssi,
                        stats.band_4g,
                        stats.rsrp_4g,
                        stats.snr_4g,
                        stats.rsrq_4g,
                        stats.imsi,
                        stats.iccid,
                        stats.imei
                    );
                    view.add_layer(info_dialog("Advanced Stats", stats_text));
                }
            );
        });
    
    let stats = STATS.read().unwrap();
    let info_5g = stats.info_5g.clone();
    let info_4g = stats.info_4g.clone();
    let devices = stats.devices.clone();
    let networks = stats.networks.clone();
    root.set_user_data(stats.clone());
    let mut theme = better_theme();
    theme.palette[PaletteColor::Highlight] = Color::Light(BaseColor::Magenta);
    theme.palette[PaletteColor::HighlightInactive] = Color::Dark(BaseColor::Magenta);
    theme.palette[PaletteColor::HighlightText] = Color::Dark(BaseColor::Black);
    root.set_theme(theme);
    let top =
        if CLIENT.model() == GatewayModel::Nokia {
            #[cfg(feature = "nokia")]
            {
                hlayout!(
                    Dialog::around(DataUsageView::new(stats.download, stats.upload).with_name("data_usage")).title("Data Usage"),
                    Dialog::around(CellStatusView::new(&info_4g, &info_5g).with_name("cell_status")).title("Connection Status")
                )
            }

            #[cfg(not(feature = "nokia"))]
            hlayout!(Dialog::around(CellStatusView::new(&info_4g, &info_5g).with_name("cell_status")).title("Connection Status"))
        }
        else {
            hlayout!(Dialog::around(CellStatusView::new(&info_4g, &info_5g).with_name("cell_status")).title("Connection Status"))
        };

    cfg_if! {
        if #[cfg(feature = "sagemcom")] {
            let layout = vlayout!(
                get_network_list(&networks).with_name("networks").scrollable(),
                Button::new("Configure Networks", net_config)
            );
        }
        else {
            let layout = get_network_list(&networks).with_name("networks").scrollable();
        }
    }

    drop(stats);
    root.add_fullscreen_layer(
        vlayout!(
            top,
            hlayout!(
                Dialog::around(get_dev_list(&devices).with_name("devices").scrollable()).title("Connected Devices"),
                Dialog::around(layout).title("Active Networks")
            ),
            StatusView::new().with_name("status")
        )
    );
    root.add_global_callback('q', |view| view.quit());

    // refresh the dashboard
    // the background thread auto updates the stats
    root.add_global_callback(Event::Refresh, move |view| {
        let mut cell_status: ViewRef<CellStatusView> = view.find_name("cell_status").unwrap();
        let mut devices: ViewRef<LinearLayout> = view.find_name("devices").unwrap();
        let mut networks: ViewRef<LinearLayout> = view.find_name("networks").unwrap();
        let mut status: ViewRef<StatusView> = view.find_name("status").unwrap();
        status.update();
        if receiver.try_recv().is_ok() {
            let stats = STATS.read().unwrap();
            // reload data usage stats
            #[cfg(feature = "nokia")]
            if let Some(mut data_usage) = view.find_name::<DataUsageView>("data_usage") {
                data_usage.update(stats.download, stats.upload);
            }

            let old_stats = view.user_data::<GatewayStats>().unwrap();
            
            // reload cell stats
            cell_status.update(&stats.info_4g, &stats.info_5g);

            // reload device list
            if old_stats.devices != stats.devices {
                *devices = get_dev_list(&stats.devices);
            }
            
            // reload network list
            if old_stats.networks != stats.networks {
                *networks = get_network_list(&stats.networks);
            }
            
            if old_stats.devices != stats.devices || old_stats.networks != stats.networks {
                *old_stats = stats.clone();
            }
        }
    });

    root.set_fps(30);
    root.run();
}