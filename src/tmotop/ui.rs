use super::*;
use std::collections::HashMap;
use cursive::{
    views::{
        LinearLayout,
        NamedView,
        Button,
        SelectView,
        ResizedView
    },
    utils::markup::StyledString,
    view::{Nameable, Scrollable}
};
use crate::{
    client::{ClientResult, tracker::SignalTracker},
    config::{DeviceConfig, GatewayConfig, GlobalConfig, Config}
};

// cursive callbacks and events

// view callbacks

// get list of connected devices with custom names assigned
pub fn device_alias_list(aliases: &HashMap<String, String>) -> NamedView<OnEventView<SelectView<(String, String)>>> {
    let mut list: SelectView<(String, String)> = SelectView::new();
    for alias in aliases {
        list.add_item(format!("{} : {}", alias.0, alias.1), (alias.0.clone(), alias.1.clone()));
    }

    list
        .wrap_with(OnEventView::new)
        .on_event(Event::Key(Key::Del), |view| {
            view.add_layer(confirm_dialog("", "Are you sure?", |view| {
                let mut hl_t = view.find_name::<OnEventView<SelectView<(String, String)>>>("device_list").unwrap();
                let hlist = hl_t.get_inner_mut();
                let idx = hlist.selected_id().unwrap();
                hlist.remove_item(idx);
                view.pop_layer();
            }));
        })
        .with_name("device_list")
}

// get the table for the connection stats log
pub fn get_table(data: (StyledString, StyledString, StyledString)) -> LinearLayout {
    vlayout!(
        TextView::new(data.0).no_wrap(),
        TextView::new(data.1).no_wrap().scrollable().scroll_x(true),
        TextView::new(data.2).no_wrap()
    )
}

// get a gateway monitor log by date
pub fn get_log(date: (u32, u32, u32), archive: bool) -> TextView { TextView::new(client::get_log(date, archive)) }

// get the list of connected devices
pub fn get_dev_list(devices: &DeviceList) -> LinearLayout {
    let mut view = LinearLayout::vertical();
    for dev in devices.iter() {
        view.add_child(ConnectedDeviceView::new(dev.clone()));
    }
    view
}

// get list of enabled networks
pub fn get_network_list(networks: &Vec<NetworkInfo>) -> LinearLayout {
    let mut view = LinearLayout::vertical();
    for network in networks {
        if network.enabled {
            view.add_child(NetworkView::new(network.clone()));
        }
    }
    view
}

// get list of enabled networks (settings view)
pub fn known_network_list(networks: &Vec<String>) -> NamedView<OnEventView<SelectView<String>>> {
    let mut list: SelectView<String> = SelectView::new();
    for network in networks {
        list.add_item_str(network)
    }
    list
        .wrap_with(OnEventView::new)
        .on_event(Event::Key(Key::Del), |view| {
            view.add_layer(confirm_dialog("", "Are you sure?", |view| {
                let mut nl_t = view.find_name::<OnEventView<SelectView<String>>>("net_list").unwrap();
                let nlist = nl_t.get_inner_mut();
                let idx = nlist.selected_id().unwrap();
                nlist.remove_item(idx);
                view.pop_layer();
            }));
        })
        .with_name("net_list")
}

// menu callbacks

// general settings view
pub fn general_settings(view: &mut Cursive) {
    let config = GlobalConfig::load();
    view.add_layer(
        settings!(
            "General Settings",
            move |view| {
                let mut status = view.find_name::<StatusView>("status").unwrap();
                let mut config2 = config.clone();
                let ip_field = view.find_name::<EditView>("ip").unwrap();
                let password_field = view.find_name::<EditView>("password").unwrap();
                let net_list = view.find_name::<OnEventView<SelectView<String>>>("net_list").unwrap();
                config2.valid_networks.clear();
                for net in net_list.get_inner().iter() {
                    config2.valid_networks.push(net.1.clone());
                }
                config2.gateway_ip = ip_field.get_content().to_string();
                config2.password = password_field.get_content().to_string();

                #[cfg(feature = "sagemcom")]
                CLIENT.set_admin_pswd(&config2.password).unwrap_or_default();

                report_error!(status, config2.save());
                view.pop_layer();
            },
            TextView::new("Gateway IP address:"),
            styled_editview_color(&config.gateway_ip, "ip", false, Color::Dark(BaseColor::Magenta)),
            TextView::new("Gateway Password:"),
            styled_editview_color(&config.password, "password", true, Color::Dark(BaseColor::Magenta)),
            labeled_checkbox_cb("Show password", "", false, |view, checked| {
                let mut password = view.find_name::<EditView>("password").unwrap();
                password.set_secret(!checked);
            }),
            TextView::new("\nKnown Networks:"),
            known_network_list(&config.valid_networks).scrollable(),
            TextView::new("\nAdd network name:"),
            styled_editview_color("", "new_net", false, Color::Dark(BaseColor::Magenta)),
            Button::new("Add", |view| {
                let new_net = view.find_name::<EditView>("new_net").unwrap();
                let mut net_list = view.find_name::<OnEventView<SelectView<String>>>("net_list").unwrap();
                net_list.get_inner_mut().add_item_str(new_net.get_content().to_string());
            })
        )
    );
}

// device settings view
pub fn device_settings(view: &mut Cursive) {
    let config = DeviceConfig::load();
    view.add_layer(
        settings!(
            "Connected Device Settings",
            move |view| {
                let mut status = view.find_name::<StatusView>("status").unwrap();
                let mut config2 = config.clone();
                config2.aliases.clear();
                let tld_field = view.find_name::<EditView>("tld").unwrap();
                let name_field = view.find_name::<EditView>("name").unwrap();
                let host_list = view.find_name::<OnEventView<SelectView<(String, String)>>>("device_list").unwrap();
                for host in host_list.get_inner().iter() {
                    config2.aliases.insert(host.1.0.to_string(), host.1.1.to_string());
                }
                config2.tld = tld_field.get_content().to_string();
                config2.gateway_name = name_field.get_content().to_string();
                report_error!(status, config2.save());
                view.pop_layer();
            },
            TextView::new("Top level domain:"),
            styled_editview_color(&config.tld, "tld", false, Color::Dark(BaseColor::Magenta)),
            TextView::new("Gateway name:"),
            styled_editview_color(&config.gateway_name, "name", false, Color::Dark(BaseColor::Magenta)),
            TextView::new("Device Aliases:"),
            device_alias_list(&config.aliases)
        )
    );
}

// gateway settings view
pub fn gateway_settings(view: &mut Cursive) {
    let config = GatewayConfig::load();
    view.add_layer(
        settings!(
            "Gateway Settings",
            move |view| {
                let mut status = view.find_name::<StatusView>("status").unwrap();
                let rhr = view.find_name::<EditView>("rhr").unwrap();
                let rmin = view.find_name::<EditView>("rmin").unwrap();
                let mut config2 = config.clone();
                config2.reboot_on_err = get_checkbox_option(view, "roe");
                config2.auto_reboot = get_checkbox_option(view, "ar");
                config2.reboot_hr = report_error!(status, rhr.get_content().parse());
                config2.reboot_min = report_error!(status, rmin.get_content().parse());
                report_error!(status, config2.save());
                view.pop_layer();
            },
            labeled_checkbox("Reboot on network issues", "roe", config.reboot_on_err),
            labeled_checkbox("Auto reboot", "ar", config.auto_reboot),
            hlayout!(
                TextView::new("Reboot Time (24 hour time):"),
                ResizedView::with_fixed_size((3, 1), styled_editview_color(&config.reboot_hr.to_string(), "rhr", false, Color::Dark(BaseColor::Magenta))),
                TextView::new(":"),
                ResizedView::with_fixed_size((3, 1), styled_editview_color(&format!("{:02}", config.reboot_min), "rmin", false, Color::Dark(BaseColor::Magenta)))
            )
        )
    );
}

// gateway monitor log
pub fn gw_monitor_log(view: &mut Cursive) {
    let dates = client::get_log_dates(false);
    let mut date_selector = SelectView::new()
        .on_select(|view, date| {
            let mut log = view.find_name::<TextView>("log").unwrap();
            log.set_content(client::get_log(*date, false));
        });

    for date in &dates {
        date_selector.add_item(format!("{:02}/{:02}/{:04}", date.1, date.2, date.0), *date)
    }

    let date = dates.first().copied();
    view.add_layer(
        Dialog::around(
            ResizedView::with_max_height(18,
                vlayout!(
                    hlayout!(
                        date_selector.with_name("d_select").scrollable(),
                        HDivider::new(),
                        get_log(date.unwrap_or((2020, 3, 1)), false).with_name("log").scrollable().max_size((50, 40))
                    ),
                    labeled_checkbox_cb("Show Archived Logs", "arclogs", false, |view, checked| {
                        let mut date_selector: ViewRef<SelectView<(u32,  u32, u32)>> = view.find_name("d_select").unwrap();
                        let dates = client::get_log_dates(checked);
                        date_selector.clear();
                        date_selector.set_on_select(move |view, date| {
                            let mut log = view.find_name::<TextView>("log").unwrap();
                            log.set_content(client::get_log(*date, checked));
                        });
                        for date in &dates {
                            date_selector.add_item(format!("{:02}/{:02}/{:04}", date.1, date.2, date.0), *date)
                        }
                    })
                )
            )
        )
            .button("Clear Logs", |view| {
                view.add_layer(confirm_dialog("Clear Logs", "Are you sure?", |view| {
                    client::archive::clear_all_logs();
                    view.pop_layer();
                    view.pop_layer();
                }));
            })
            .button("Archive Logs", |view| {
                client::archive::archive_gwmonlogs();
                view.pop_layer();
            })
            .dismiss_button("Back")
            .title("Gateway Monitor Log")
            .wrap_with(OnEventView::new)
            .on_event(Event::Key(Key::Esc), |v| { v.pop_layer(); })
    )
}

// connections stats log
pub fn conn_stats_log(view: &mut Cursive) {
    let sig_tracker = SignalTracker::get(false);
    let dates = sig_tracker.get_dates();
    let first_date =  dates.first().copied().unwrap_or((1970, 1, 1));
    let sv_sig_tracker = sig_tracker.clone();
    let entries = sig_tracker.get_formatted_entries(first_date);
    let mut date_selector: SelectView<(u32, u32, u32)> = SelectView::new()
        .on_select(move |view, date| {
            let mut table = view.find_name::<LinearLayout>("table").unwrap();
            let entries = sv_sig_tracker.get_formatted_entries(*date);
            *table = get_table(entries);
        });
    for date in dates {
        date_selector.add_item(format!("{:02}/{:02}/{:04}", date.1, date.2, date.0), date)
    }

    view.add_layer(
        Dialog::around(
            vlayout!(
                hlayout!(
                    date_selector.with_name("d_select2").scrollable(),
                    get_table(entries).with_name("table")
                ),
                labeled_checkbox_cb("Show Archived Stats", "arcstats", false, |view, checked| {
                    let sig_tracker = SignalTracker::get(checked);
                    let mut date_selector: ViewRef<SelectView<(u32,  u32, u32)>> = view.find_name("d_select2").unwrap();
                    let dates = sig_tracker.get_dates();
                    date_selector.clear();
                    date_selector.set_on_select(move |view, date| {
                        let mut table = view.find_name::<LinearLayout>("table").unwrap();
                        let entries = sig_tracker.get_formatted_entries(*date);
                        *table = get_table(entries);
                    });

                    for date in &dates {
                        date_selector.add_item(format!("{:02}/{:02}/{:04}", date.1, date.2, date.0), *date)
                    }
                })
            )
        )
            .button("Archive Stats", |view| {
                client::archive::archive_sig_tracker();
                view.pop_layer();
            })
            .dismiss_button("Back")
            .title("Connection Stats Log")
            .wrap_with(OnEventView::new)
            .on_event(Event::Key(Key::Esc), |v| { v.pop_layer(); })
    )
}

// connection stats visual
pub fn conn_stats_vis(view: &mut Cursive) {
    let sig_tracker = SignalTracker::get(false);
    let dates = sig_tracker.get_dates();
    let sv_sig_tracker = sig_tracker.clone();
    let mut date_selector: SelectView<(u32, u32, u32)> = SelectView::new()
        .on_select(move |view, date| {
            let mut graph = view.find_name::<ConnStatVisual>("graph").unwrap();
            let mut new_entries = Vec::new();
            for entry in sv_sig_tracker.iter_entries_by_date(*date) {
                new_entries.push((entry.info_5g.strength, entry.info_4g.strength, entry.time.0, entry.reboot))
            }

            *graph = ConnStatVisual::new(new_entries);
        });
    for date in &dates {
        date_selector.add_item(format!("{:02}/{:02}/{:04}", date.1, date.2, date.0), *date)
    }

    let mut new_entries = Vec::new();
    let first_date =
        if let Some(date) = dates.first() { *date }
        else { (1970, 1,1) };
    for entry in sig_tracker.iter_entries_by_date(first_date) {
        new_entries.push((entry.info_5g.strength, entry.info_4g.strength, entry.time.0, entry.reboot));
    }

    let style_5g = ColorStyle::new(Color::TerminalDefault, Color::Light(BaseColor::Cyan));
    let style_5g_r = ColorStyle::new(Color::TerminalDefault, Color::Light(BaseColor::Red));
    let style_4g = ColorStyle::new(Color::TerminalDefault, Color::Dark(BaseColor::Cyan));
    let style_4g_r = ColorStyle::new(Color::TerminalDefault, Color::Dark(BaseColor::Red));
    let mut legend = StyledString::from("Legend:\n");
    legend.append(StyledString::styled(" ", style_5g));
    legend.append(StyledString::styled(" ", style_5g_r));
    legend.append(" 5G Signal\n");
    legend.append(StyledString::styled(" ", style_4g));
    legend.append(StyledString::styled(" ", style_4g_r));
    legend.append(" 4G (LTE) Signal\n\nRed indicates the gateway was rebooted");

    view.add_layer(
        Dialog::around(
            ResizedView::with_max_height(23,
                vlayout!(
                    hlayout!(
                        date_selector.with_name("d_select3").scrollable(),
                        HDivider::new(),
                        vlayout!(
                            hlayout!(
                                ConnStatVisual::get_info_view(),
                                ResizedView::with_max_size((41, 18),
                                    ConnStatVisual::new(new_entries)
                                        .with_name("graph")
                                        .scrollable()
                                        .scroll_x(true)
                                )
                            ),
                            TextView::new(legend)
                        )
                    ),

                    labeled_checkbox_cb("Show Archived Stats", "arcstats", false, |view, checked| {
                        let sig_tracker = SignalTracker::get(checked);
                        let mut date_selector: ViewRef<SelectView<(u32,  u32, u32)>> = view.find_name("d_select3").unwrap();
                        let dates = sig_tracker.get_dates();
                        date_selector.clear();
                        date_selector.set_on_select(move |view, date| {
                            let mut graph = view.find_name::<ConnStatVisual>("graph").unwrap();
                            let mut new_entries = Vec::new();
                            for entry in sig_tracker.iter_entries_by_date(*date) {
                                new_entries.push((entry.info_5g.strength, entry.info_4g.strength, entry.time.0, entry.reboot))
                            }

                            *graph = ConnStatVisual::new(new_entries);
                        });

                        for date in &dates {
                            date_selector.add_item(format!("{:02}/{:02}/{:04}", date.1, date.2, date.0), *date)
                        }
                    })
                )
            )
        )
            .title("Connection Stats Visual")
            .dismiss_button("Back")
            .wrap_with(OnEventView::new)
            .on_event(Event::Key(Key::Esc), |v| { v.pop_layer(); })
    )
}

// network settings (sagemcom only)
#[cfg(feature = "sagemcom")]
pub fn net_config(view: &mut Cursive) {
    load_resource(view,
        "Please wait...", "Getting network settings...",
        || CLIENT.get_networks(),
        |view, res: ClientResult<Vec<NetworkInfo>>| {
            let mut status: ViewRef<StatusView> = view.find_name("status").unwrap();
            let networks = report_error!(status, res);
            let mut net_list = SelectView::new()
                .on_submit(|view, network: &NetworkInfo| {
                    let network = network.clone();
                    view.add_layer(
                        settings!(
                            "Edit Network",
                            move |view| {
                                let mut net_list = view.find_name::<SelectView<NetworkInfo>>("net_list").unwrap();
                                let name_field = view.find_name::<EditView>("net_name").unwrap();
                                let pswd_field = view.find_name::<EditView>("net_password").unwrap();
                                let name = name_field.get_content().to_string();
                                let password = pswd_field.get_content().to_string();

                                if name.contains(' ') || name.is_empty() || name.chars().count() > 28 {
                                    view.add_layer(info_dialog("Error", "Name can't contain spaces and must be 1-28 characters!"));
                                    return;
                                }

                                if password.chars().count() < 8 || password.chars().count() > 63 {
                                    view.add_layer(info_dialog("Error", "Password must be at 8-63 characters!"));
                                    return;
                                }

                                for (i, (_, net)) in net_list.iter_mut().enumerate() {
                                    if net.id == network.id {
                                        *net = NetworkInfo {
                                            id: format!("WLAN{i}"),
                                            name,
                                            password,
                                            net_security: "WPA2".to_string(),
                                            freq2_4g: get_checkbox_option(view, "band_2_4g"),
                                            freq5g: get_checkbox_option(view, "band_5g"),
                                            enabled: true,

                                            #[cfg(feature = "nokia")]
                                            download: 0,

                                            #[cfg(feature = "nokia")]
                                            upload: 0
                                        };
                                        break;
                                    }
                                }
                                view.pop_layer();
                            },
                            TextView::new("Network Name:"),
                            styled_editview_color(&network.name, "net_name", false, Color::Dark(BaseColor::Magenta)),
                            TextView::new("Network Password:"),
                            styled_editview_color(&network.password, "net_password", true, Color::Dark(BaseColor::Magenta)),
                            labeled_checkbox_cb("Show password", "", false, |view, checked| {
                                let mut password = view.find_name::<EditView>("net_password").unwrap();
                                password.set_secret(!checked);
                            }),
                            labeled_checkbox("2.4GHz Band", "band_2_4g", true),
                            labeled_checkbox("5GHz Band", "band_5g", false)
                        )
                    )
                });

            for network in &networks {
                net_list.add_item(&network.name, network.clone());
            }

            view.add_layer(
                Dialog::around(net_list.with_name("net_list"))
                    .title("Wifi Network Settings")
                    .button("New Network", |view| {
                        view.add_layer(
                            settings!(
                                "New Network",
                                |view| {
                                    let mut net_list = view.find_name::<SelectView<NetworkInfo>>("net_list").unwrap();
                                    let name_field = view.find_name::<EditView>("new_net_name").unwrap();
                                    let pswd_field = view.find_name::<EditView>("new_net_password").unwrap();
                                    let name = name_field.get_content().to_string();
                                    let password = pswd_field.get_content().to_string();

                                    if name.contains(' ') || name.is_empty() || name.chars().count() > 28 {
                                        view.add_layer(info_dialog("Error", "Name can't contain spaces and must be 1-28 characters!"));
                                        return;
                                    }

                                    if password.chars().count() < 8 || password.chars().count() > 63 {
                                        view.add_layer(info_dialog("Error", "Password must be 8-63 characters!"));
                                        return;
                                    }

                                    net_list.add_item(name_field.get_content().to_string(), NetworkInfo {
                                        id: String::new(),
                                        name,
                                        password,
                                        net_security: "WPA2".to_string(),
                                        freq2_4g: get_checkbox_option(view, "band_2_4g"),
                                        freq5g: get_checkbox_option(view, "band_5g"),
                                        enabled: true,

                                        #[cfg(feature = "nokia")]
                                        download: 0,

                                        #[cfg(feature = "nokia")]
                                        upload: 0
                                    });
                                    view.pop_layer();
                                },
                                TextView::new("Network Name:"),
                                styled_editview_color("", "new_net_name", false, Color::Dark(BaseColor::Magenta)),
                                TextView::new("Network Password:"),
                                styled_editview_color("", "new_net_password", true, Color::Dark(BaseColor::Magenta)),
                                labeled_checkbox_cb("Show password", "", false, |view, checked| {
                                    let mut password = view.find_name::<EditView>("new_net_password").unwrap();
                                    password.set_secret(!checked);
                                }),
                                labeled_checkbox("2.4GHz Band", "band_2_4g", true),
                                labeled_checkbox("5GHz Band", "band_5g", false)
                            )
                        );
                    })
                    .button("Save", |view| {
                        view.add_layer(
                            ResizedView::with_fixed_width(30,
                                Dialog::text("Your Wifi will be down for a couple moments while the gateway configures the wifi settings!")
                                    .title("Info")
                                    .button("Ok", |view| {
                                        let net_list = view.find_name::<SelectView<NetworkInfo>>("net_list").unwrap();
                                        let mut networks = Vec::new();
                                        for (_, net) in net_list.iter() {
                                            networks.push(net.clone());
                                        }
                                        load_resource(view,
                                            "Please wait...", "Configuring gateway...",
                                            move || CLIENT.set_networks(&networks).unwrap_or(()),
                                            |view, _| view.quit()
                                        );
                                    })
                                    .dismiss_button("Cancel")
                            )

                        );
                    })
                    .dismiss_button("Back")
                    .wrap_with(OnEventView::new)
                    .on_event(Event::Key(Key::Esc), |v| { v.pop_layer(); })
                    .on_event(Event::Key(Key::Del), |view| {
                        view.add_layer(confirm_dialog("Delete Network", "Are you sure?", |view| {
                            let mut net_list = view.find_name::<SelectView<NetworkInfo>>("net_list").unwrap();
                            if let Some(selected) = net_list.selected_id() {
                                net_list.remove_item(selected);
                                view.pop_layer();
                            }
                        }));
                    })
            )
        }
    );
}