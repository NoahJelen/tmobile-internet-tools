use std::{
    env,
    collections::HashMap
};
use serde::{Deserialize, Serialize};
use crate::client;
pub use rust_utils::config::Config;

#[derive(Deserialize, Serialize, Clone)]
pub struct GlobalConfig {
    // ip address of the gateway (should hopefully always be 192.168.12.1)
    pub gateway_ip: String,

    // web interface username (should hopefully always be 'admin')
    pub username: String,

    // web interface password
    pub password: String,

    // list of network names that the gateway is associated with 
    // the management computer connects to
    pub valid_networks: Vec<String>
}

impl Default for GlobalConfig {
    fn default() -> GlobalConfig {
        let valid_networks = client::connected_networks().iter().map(|val| val.0.clone()).collect();
        GlobalConfig {
            gateway_ip: "192.168.12.1".to_string(),
            username: "admin".to_string(),
            password: "".to_string(),
            valid_networks
        }
    }
}

impl Config for GlobalConfig {
    const FILE_NAME: &'static str = "global.toml";
    fn get_save_dir() -> String { format!("{}/.config/tmo-tools", env::var("HOME").expect("Where the hell is your home folder?!")) }
}

// config file for the gateway monitoring process
#[derive(Deserialize, Serialize, Clone)]
pub struct GatewayConfig {
    // should the gateway reboot when network issues are detected?
    pub reboot_on_err: bool,

    // should the gateway reboot at a specific time?
    pub auto_reboot: bool,

    // hour to reboot the gateway at
    pub reboot_hr: u32,

    // minute to reboot the gateway at
    pub reboot_min: u32,
}

impl Default for GatewayConfig {
    fn default() -> GatewayConfig {
        GatewayConfig {
            reboot_on_err: true,
            auto_reboot: true,
            reboot_hr: 2,
            reboot_min: 0
        }
    }
}

impl Config for GatewayConfig {
    const FILE_NAME: &'static str = "gateway.toml";
    fn get_save_dir() -> String { format!("{}/.config/tmo-tools", env::var("HOME").expect("Where the hell is your home folder?!")) }
}

#[derive(Deserialize, Serialize, Clone)]
pub struct DeviceConfig {
    // top level domain
    pub tld: String,

    // gateway name
    pub gateway_name: String,

    // alias to real names
    pub aliases: HashMap<String, String>
}

impl Default for DeviceConfig {
    fn default() -> DeviceConfig {
        DeviceConfig {
            tld: ".local".to_owned(),
            gateway_name: "tmo-gateway".to_owned(),
            aliases: HashMap::new()
        }
    }
}

impl Config for DeviceConfig {
    const FILE_NAME: &'static str = "devices.toml";
    fn get_save_dir() -> String { format!("{}/.config/tmo-tools", env::var("HOME").expect("Where the hell is your home folder?!")) }
}