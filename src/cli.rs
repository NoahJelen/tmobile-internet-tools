use std::{env, io, process};
use crate::{
    client::*,
    stats::*,
    CLIENT,
    VERSION,
    devices::DeviceList
};
use colored::Colorize;
use rust_utils::config::Config;
use tmo_tools::config::GlobalConfig;

#[cfg(feature = "sagemcom")]
use qrcode::render::unicode;

const RATING: [&str; 5] = [
    "Poor",
    "Weak",
    "Good",
    "Very Good",
    "Excellent"
];

pub fn parse_args() {
    // get the command line arguments of the program
    let args: Vec<String> = env::args().collect();
    
    if args.len() <= 1 {
        show_stats(&get_stats());
        return;
    }

    match args[1].as_str() {
        "-v" => println!("T-Mobile Home Internet Tools\nVersion {VERSION}"),
        "devices" => show_devices(),
        "stats" => show_stats(&get_stats()),
        "networks" | "net" => show_networks(&get_stats(), args.get(2).unwrap_or(&"".to_string()) == "-q"),
        "connection" | "con" => show_con_stat(&get_stats()),
        "info" => show_info(),
        "login" => login(),

        #[cfg(feature = "nokia")]
        "data-usage" | "data" => show_data(&get_stats()),

        "-h" => {
            println!("tmocli help:");
            println!("Use tmocli -h to show this again.");
            println!("Use tmocli -v to show current version.");
            println!("Subcommands:");
            println!("  devices: list all devices connected to gateway");
            println!("  device <name, IP, or MAC address>: find device connected to gateway by name, IP address or MAC address");
            println!("  stats: show the gateway's stats");
            println!("  data-usage, data: show the gateway's data usage");
            println!("  networks, net: show the gateway's active networks");
            println!("      pass -q to show wifi QR codes");
            println!("  connection, con: show the gateway's cell connection status");
            println!("  info: show general info about the gateway");
            println!("  reboot: reboot the gateway");
        },

        "device" => {
            let arg = match args.get(2) {
                Some(a) => a.to_string(),
                None => {
                    println!("Please specify a device name, IP address, or MAC address!");
                    return;
                }
            };
            show_device(arg);
        },

        "reboot" => {
            let arg = match args.get(2) {
                Some(a) => a,
                None => ""
            };

            ctrlc::set_handler(|| {
                println!("Reboot Cancelled!");
                process::exit(0);
            }).expect("Error setting ^C handler!");

            if arg != "-y" {
                let mut ans = String::new();
                println!("Your network will be down for about 5 minutes. Reboot gateway? (N/y)");
                io::stdin().read_line(&mut ans).unwrap();
                ans.retain(|c| c !='\n');
                if ans.as_str() != "y" {
                    println!("Reboot Cancelled!");
                    return;
                }
            }

            println!("Rebooting gateway...");
            if let Err(why) = CLIENT.reboot_gateway_no_log() {
                eprintln!("Error rebooting gateway: {why}");
            };
        },

        _ => eprintln!("Invalid option!")
    }
}

fn show_info() {
    match CLIENT.get_info() {
        Ok(i) => println!("Gateway Info:\n{i}"),
        Err(why) => eprintln!("Error getting gateway info: {why}")
    }
}

fn show_stats(stats: &GatewayStats) {
    #[cfg(feature = "nokia")]
    show_data(stats);

    show_networks(stats, false);
    show_con_stat(stats);
}

fn show_networks(stats: &GatewayStats, show_codes: bool) {
    let net_stat = check_net_env(true);
    print!("Network Status: ");
    if net_stat == 0 {
        println!("{}", "Ok".bright_green())
    }
    else {
        println!("{}", "Offline".bright_red());
        println!("Please reboot your gateway.")
    }
    println!();
    println!("Active Networks:\n");
    for network in &stats.networks {
        if network.enabled {
            println!("{}", network.name.bright_white());

            #[cfg(feature = "sagemcom")]
            {
                println!("Password: {}", network.password);
                println!("Security: {}", network.net_security);
            }
            println!("ID: {}", network.id);

            #[cfg(feature = "nokia")]
            {
                let download_fmt = format!("Down: {}", format_bytes(network.download)).bright_magenta();
                let upload_fmt = format!("Up: {}", format_bytes(network.upload)).bright_cyan();
                println!("{download_fmt} {upload_fmt}");
            }

            #[cfg(feature = "sagemcom")]
            {
                if show_codes {
                    let qr_code = net_qr_code(&network.name, &network.password, &network.net_security);
                    let qr_print = qr_code.render::<unicode::Dense1x2>()
                        .dark_color(unicode::Dense1x2::Light)
                        .light_color(unicode::Dense1x2::Dark)
                        .build();

                    println!("{}", qr_print.bright_white());
                }
            }
            println!();
        }
    }
}

#[cfg(feature = "nokia")]
fn show_data(stats: &GatewayStats) {
    println!("Data Usage:");
    let download_fmt = format!("Download: {}", format_bytes(stats.download)).bright_magenta();
    let upload_fmt = format!("Upload: {}", format_bytes(stats.upload)).bright_cyan();
    let total_fmt = format!("Total: {}", format_bytes(stats.download + stats.upload));
    println!("{download_fmt} {upload_fmt} {total_fmt}\n");
}

fn show_con_stat(stats: &GatewayStats) {
    println!("Connection Status:\n");
    println!("5G:");
    println!("Band: {}", stats.info_5g.band);
    println!("Strength: {} dBm ({})\n", stats.info_5g.strength, RATING[(stats.info_5g.bars - 1) as usize]);
    println!("LTE (4G):");
    println!("Band: {}", stats.info_4g.band);
    println!("Strength: {} dBm ({})", stats.info_4g.strength, RATING[(stats.info_4g.bars - 1) as usize]);
}

fn show_device(arg: String) {
    let devices = get_devices();
    for dev in devices.iter() {
        if dev.get_name() == arg || dev.get_ip() == arg || dev.get_mac_addr() == arg{
            println!("{}", dev.get_name().bright_magenta());
            println!("{}", dev.get_ip());
            if !dev.is_gateway() {
                println!("{}", dev.get_mac_addr());
            }
        }
    }
}

fn show_devices() {
    let devices = get_devices();
    for host in devices.iter() {
        println!("{}", host.get_name().bright_magenta());
        println!("{}", host.get_ip());
        if !host.is_gateway() {
            println!("{}", host.get_mac_addr());
        }
        println!();
    }
    print!("\x08");
}

fn get_devices() -> DeviceList {
    match CLIENT.get_devices() {
        Ok(h) => h,
        Err(why) => {
            let msg = format!("Error: Unable to get devices connected to gateway! {why}").bright_red();
            eprintln!("{msg}");
            process::exit(255);
        }
    }
}

// get the gateway's stats
fn get_stats() -> GatewayStats {
    match CLIENT.get_all_stats() {
        Ok(s) => s,
        Err(why) => {
            let msg = format!("Error: Unable to get gateway stats! {why}").bright_red();
            eprintln!("{msg}");
            process::exit(255);
        }
    }
}

// log into gateway
fn login() {
    println!("Gateway Password:");
    let mut pwd_input = String::new();
    let stdin = io::stdin();
    stdin.read_line(&mut pwd_input).unwrap();
    pwd_input.pop();
    println!("Logging into gateway...");
    let mut config = GlobalConfig::load();
    config.password = pwd_input;
    config.save().unwrap();

    // was the login successful?
    if let Err(why) = CLIENT.get_info() {
        println!("Error logging into gateway: {why}");
    }
}