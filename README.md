[![Crates.io](https://img.shields.io/crates/v/tmobile-internet-tools)](https://crates.io/crates/tmobile-internet-tools)
[![AUR version](https://img.shields.io/aur/version/tmobile-internet-tools)](https://aur.archlinux.org/packages/tmobile-internet-tools)
# T-Mobile Home Internet Tools
![](icon.png)

Set of programs to make managing T-Mobile 5G Wifi Gateways easier

Notice: This has only been tested and known to work with the Nokia "trashcan" and Sagemcom gateways!

## Nokia "Trashcan" Gateway:
![](trashcan.png)

## Sagemcom Gateway
![](sagemcom.png)

## The programs:

`tmocli`: Command line interface for working with the gateway

![](screenshots/tmocli.png)

`tmotop`: Text interface for working with the gateway

![](screenshots/tmotop.png)

`tmo-webui` Mobile friendly web interface for working with the gateway

![](screenshots/webui.png)

`gatewaymon`: Background process that monitors the gateway for network issues (reboots the gateway if needed) and monitors the gateway's cell signal

`tmo-hosts`: (Work in progress) Background process that makes other computers on the network acessible by their hostnames

## Installation
### Arch Linux and others based on it
Use your AUR helper to install `tmobile-internet-tools`

If building manually with cargo, add to the build arguments `--features nokia` to enable nokia gateway support and `--no-default-features` to disable sagemcom support.

These options are also availble in the Arch Linux PKGBUILD.

## Troubleshooting
"Error: Not authorized" errors coming from nmcli in gatewaymon: Make sure your user is in the 'network' group and use `visudo` to add the following to `/etc/sudoers`: `%network ALL=(ALL) NOPASSWD: /usr/bin/nmcli`

## Disclaimer: I am in no way affiliated with T-Mobile. This program is my own work.

## Planned Features:
- [ ] Background process that tracks the IPs of other hosts (These gateways are DHCP only)
- [x] Allow web interface to auto-refresh gateway info
- [x] Text UI to show metrics and settings
- [x] Background process that reboots the gateway when needed
- [x] Command line interface to work with the gateway
- [x] Web interface that shows metrics and settings
