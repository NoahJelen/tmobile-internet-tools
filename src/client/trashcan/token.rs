use std::{
    sync::Arc,
    collections::HashMap
};
use rand::{Rng, thread_rng};
use reqwest::{
    Url,
    cookie::Jar,
    blocking::Client
};
use rust_utils::logging::LogLevel;
use sha2::{Sha256, Digest};
use crate::{
    LOG,
    DEBUG,
    PRINT_STDOUT,
    client::{ClientError, parse_login_json}
};

pub struct AuthToken<'a> {
    raw_token: String,
    sid_cookie: String,
    lsid_cookie: String,
    client: &'a Client
}

#[allow(clippy::new_without_default)]
impl<'a> AuthToken<'a> {
    // create a new token using a username and password
    pub fn new(client: &'a Client, username: &str, password: &str) -> Result<AuthToken<'a>, ClientError> {
        let nonce_response = match client.get("http://192.168.12.1/login_web_app.cgi?nonce").send() {
            Ok(response) => response.text().unwrap(),
            Err(why) => {
                LOG.line(LogLevel::Error, format!("Error logging into gateway: {why}"), false);
                return Err(ClientError::GatewayUnreachable(why.to_string()))
            }
        };

        let nonce_json = parse_login_json(&nonce_response)?;

        // get the username and password hash
        let user_pass_hash = sha_256_hash(username, &password.to_lowercase());

        // hash the username+password hash and the nonce key together
        let nonce_key = nonce_json["nonce"].to_string();
        let passwd_nonce_hash = encode_as_url(sha_256_hash(&user_pass_hash, &nonce_key));

        // hash the username and nonce key together
        let username_nonce_hash = encode_as_url(sha_256_hash(username, &nonce_key));

        // get the random key number hash
        let rkey = nonce_json["randomKey"].to_string();
        let rkey_hash = encode_as_url(sha_256_hash(&rkey, &nonce_key));

        // generate the crypto keys
        let mut rand = thread_rng();
        let mut enckey1: Vec<u8> = Vec::new();
        let mut enckey2: Vec<u8> = Vec::new();

        for _ in 0..16 {
            enckey1.push(rand.gen());
            enckey2.push(rand.gen());
        }

        let mut request = HashMap::new();
        request.insert("userhash", username_nonce_hash);
        request.insert("RandomKeyhash", rkey_hash);
        request.insert("response", passwd_nonce_hash);
        request.insert("nonce", encode_as_url(nonce_key));
        request.insert("enckey", encode_as_url(base64::encode(enckey1)));
        request.insert("enciv", encode_as_url(base64::encode(enckey2)));

        let response = match client.post("http://192.168.12.1/login_web_app.cgi").form(&request).send() {
            Ok(r) => r,
            Err(why) => return Err(ClientError::GatewayUnreachable(why.to_string()))
        };

        let mut sid_cookie: String = String::new();
        let mut lsid_cookie: String = String::new();
        for cookie in response.cookies() {
            match cookie.name() {
                "sid" => sid_cookie = cookie.value().to_string(),
                "lsid" => lsid_cookie = cookie.value().to_string(),
                _ => { }
            }
        }
        let response_json = parse_login_json(&response.text().unwrap())?;

        Ok(AuthToken {
            raw_token: response_json["token"].to_string(),
            sid_cookie,
            lsid_cookie,
            client
        })
    }

    // add the cookies of this token to the client's cookie store
    pub fn put_cookies_in_jar(&self, jar: &Arc<Jar>) {
        let sid = format!("sid={}; Domain=192.168.12.1", self.sid_cookie);
        let lsid = format!("lsid={}; Domain=192.168.12.1", self.lsid_cookie);
        let url = Url::parse("http://192.168.12.1").unwrap();
        jar.add_cookie_str(&sid, &url);
        jar.add_cookie_str(&lsid, &url);
    }

    // get the session cookie needed to make requests
    pub fn get_data(&self) -> HashMap<&'static str, String> {
        let mut data = HashMap::new();
        data.insert("csrf_token", self.raw_token.clone());
        data
    }
}

// log out the client when this token gets dropped
impl<'a> Drop for AuthToken<'a> {
    fn drop(&mut self) {
        LOG.line(*DEBUG, "Logging client out...", *PRINT_STDOUT);
        if let Err(why) = self.client.post("http://192.168.12.1/login_web_app.cgi?out").form(&self.get_data()).send() {
            LOG.line(LogLevel::Error, format!("Error logging out client: {why}"), *PRINT_STDOUT);
        }
    }
}

fn sha_256_hash(val1: &str, val2: &str) -> String {
    let mut hasher = Sha256::new();
    let string_to_hash = format!("{val1}:{val2}");
    hasher.update(&string_to_hash);
    let output = hasher.finalize();
    base64::encode(&output[..])
}

fn encode_as_url(b64: String) -> String {
    let mut out = String::new();
    for c in b64.chars() {
        match c {
            '+' => out.push('-'),
            '/' => out.push('_'),
            '=' => out.push('.'),
            _ => out.push(c),
        }
    }
    out
}