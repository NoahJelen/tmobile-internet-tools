use std::{
    env, thread, fmt, process,
    sync::{RwLock, LockResult},
    time::Duration
};
use actix_files::Files;
use actix_web::{web, App, HttpServer, HttpResponse};
use serde_derive::Deserialize;
use lazy_static::lazy_static;
use rust_utils::logging::LogLevel;
use tmo_tools::{
    CLIENT, LOG,
    stats::{GatewayStats, GatewayInfo},
    config::{DeviceConfig, GatewayConfig, GlobalConfig, Config}
};

mod pages;

lazy_static! {
    static ref STATS: RwLock<Option<GatewayStats>> = RwLock::new(None);
    static ref INFO: RwLock<Option<GatewayInfo>> = RwLock::new(None);
}

#[derive(Deserialize)]
pub struct SettingsForm {
    // general settings
    gw_ip: String,
    password: String,
    new_net: String,

    // gateway settings
    reboot_on_err: Option<String>,
    auto_reboot: Option<String>,
    reboot_hr: u32,
    reboot_min: u32,

    // connected devices settings
    tld: String,
    gw_name: String
}

#[derive(Debug)]
pub struct ValueError;
impl fmt::Display for ValueError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { write!(f, "Can't get value!") }
}

impl actix_web::ResponseError for ValueError { }

// this is like Result::unwrap(), but exits the entire process if it fails to return a value
trait ExitOnPoison<T> {
    fn ok_or_exit(self) -> T;
}

impl<T> ExitOnPoison<T> for LockResult<T> {
    fn ok_or_exit(self) -> T {
        if let Ok(val) = self { val }
        else {
            LOG.line(LogLevel::Error, "An internal error occurred!", true);
            process::exit(101);
        }
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    LOG.report_panics(true);
    LOG.line_basic("Starting up T-Mobile Gateway Monitor Web UI...", true);

    // this thread refreshes the stats 
    thread::spawn(|| {
        loop {
            if let Ok(mut prev_stats) = STATS.try_write() {
                if let Ok(mut prev_info) = INFO.try_write() {
                    let stats = CLIENT.get_all_stats();
                    let info = CLIENT.get_info();
                    *prev_stats = match stats {
                        Ok(ref s) => Some(s.clone()),
                        Err(why) => {
                            LOG.line(LogLevel::Error, format!("Error getting gateway stats: {why}"), true);
                            None
                        }
                    };

                    *prev_info = match info {
                        Ok(i) => Some(i.clone()),
                        Err(why) => {
                            LOG.line(LogLevel::Error, format!("Error getting gateway info: {why}"), true);
                            None
                        }
                    };
                }
            }
            thread::sleep(Duration::from_secs(10));
        }
    });

    ctrlc::set_handler(|| {
        LOG.line_basic("Shutting down...", true);
        process::exit(0);
    }).expect("Error setting Ctrl-C handler");

    LOG.line_basic("Startup complete", true);

    if env::set_current_dir("webui/").is_err() {
        env::set_current_dir("/usr/share/tmobile-internet-tools/webui/").unwrap();
    };
    HttpServer::new(|| {
        let sys = App::new()
            .route("/", web::get().to(pages::index))
            .route("/help", web::get().to(pages::help))
            .route("/adv_stats", web::get().to(pages::adv_stats))
            .route("/conn_stats", web::get().to(pages::conn_stats))
            .route("/diag", web::get().to(pages::diag))
            .route("/diag/gatewaymon_log", web::get().to(pages::gatewaymon_log))
            .route("/diag/gateway_stats_log", web::get().to(pages::gateway_stats_log))
            .route("/config", web::get().to(pages::config))
            .route("/config/save_cfg", web::post().to(save))
            .route("/config/reboot_gateway", web::get().to(reboot_gateway))

            // REST API for refreshing the stats
            .route("/conn_stats_json", web::get().to(pages::conn_stats_json))
            .route("/gateway_info_json", web::get().to(pages::gateway_info_json))
            .route("/dev_list_json", web::get().to(pages::dev_list_json));

        #[cfg(feature = "sagemcom")]
        let sys = sys.route("/wifi_qr", web::get().to(pages::wifi_qr_code));

        sys.service(Files::new("/", ".").show_files_listing())
    })
        .bind(("0.0.0.0", 7700))?
        .run()
        .await
}

// request handlers

// reboot the gateway
async fn reboot_gateway() -> HttpResponse {
    let res = client_req(|| CLIENT.reboot_gateway(true));
    if res.is_err() { HttpResponse::from_error(res.unwrap_err()) }
    else { HttpResponse::Ok().finish() }
}

// save config
async fn save(web::Form(form): web::Form<SettingsForm>) -> HttpResponse {
    let mut config = GlobalConfig::load();
    let mut gw_config = GatewayConfig::load();
    let mut dev_config = DeviceConfig::load();
    config.gateway_ip = form.gw_ip;
    config.password = form.password;

    if !form.new_net.is_empty() {
        config.valid_networks.push(form.new_net);
    }
    LOG.line_basic("Saving global settings...", true);

    #[cfg(feature = "sagemcom")]
    {
        let new_adm_pwd = config.password.clone();
        client_req(move || CLIENT.set_admin_pswd(&new_adm_pwd).unwrap_or_default());
    }
    config.save().expect("Unable to save settings!");
    gw_config.reboot_on_err = form.reboot_on_err.is_some();
    gw_config.auto_reboot = form.auto_reboot.is_some();
    gw_config.reboot_hr = form.reboot_hr;
    gw_config.reboot_min = form.reboot_min;
    LOG.line_basic("Saving gateway settings...", true);
    gw_config.save().expect("Unable to save settings!");
    dev_config.tld = form.tld;
    dev_config.gateway_name = form.gw_name;
    LOG.line_basic("Saving device settings...", true);
    dev_config.save().expect("Unable to save settings!");
    LOG.line_basic("Reloading settings...", true);

    // the "created" resource is the reloaded settings
    HttpResponse::Created().append_header(("Location", "/config"))
        .content_type("text/html")
        .body(
            "<!DOCTYPE html>\n\
            <html>\n\
                <head>\n\
                    <link rel=\"stylesheet\" href=\"/styles.css\">\n\
                    <meta http-equiv=\"refresh\" content=\"0; url='/config'\"/>\n\
                </head>\n\
                <body>\n\
                    <a href=\"/config\">Reload settings</a>\n\
                </body>\n\
            </html>"
        )
}

// the raw gateway request must be made on another thread, otherwise the server crashes
fn client_req<T: Send + Sync + 'static, F: FnOnce() -> T + Send + Sync + 'static>(request: F) -> T {
    let thread = thread::spawn(request);
    thread.join().unwrap()
}